Go Monero RPC Client
====================
This repo forks the [Go Monero RPC Client](https://github.com/monero-ecosystem/go-monero-rpc-client).
We added some golang RPC calls to support [Thorchain Customised Monero](https://gitlab.com/thorchain/tss/monero-sign).

## Interface Updates

To support a customised Monero, we added the following interfaces.

```go	

// CheckTransaction chect k whether the transaction we received has the same destination and amount
CheckTransaction(req *RequestCheckTransaction) (resp *ResponseCheckTransaction, err error)

```

These interfaces are wraps of the RPC interfaces implemented in  [Thorchain Customised Monero](https://gitlab.com/thorchain/tss/monero-sign).
