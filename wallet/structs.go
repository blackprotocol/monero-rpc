package wallet

// Destination helper structs
type Destination struct {
	// Amount to send to each destination, in atomic units.
	Amount uint64 `json:"amount"`
	// Destination public address.
	Address string `json:"address"`
}

type SignedKeyImage struct {
	KeyImage  string `json:"key_image"`
	Signature string `json:"signature"`
}

// RequestGetBalance used by GetBalance
type RequestGetBalance struct {
	AccountIndex   uint64   `json:"account_index"`   // Return balance for this account.
	AddressIndices []uint64 `json:"address_indices"` // (Optional) Return balance detail for those subaddresses.
}

// ResponseGetBalance used by GetBalance
type ResponseGetBalance struct {
	Balance              uint64 `json:"balance"`                // The total balance of the current monero-wallet-rpc in session.
	UnlockedBalance      uint64 `json:"unlocked_balance"`       // Unlocked funds are those funds that are sufficiently deep enough in the Monero blockchain to be considered safe to spend.
	MultisigImportNeeded bool   `json:"multisig_import_needed"` // True if importing multisig data is needed for returning a correct balance.
	PerSubaddress        []struct {
		AddressIndex      uint64 `json:"address_index"`       // Index of the subaddress in the account.
		Address           string `json:"address"`             // Address at this index. Base58 representation of the public keys.
		Balance           uint64 `json:"balance"`             // Balance for the subaddress (locked or unlocked).
		UnlockedBalance   uint64 `json:"unlocked_balance"`    // Unlocked balance for the subaddress.
		Label             string `json:"label"`               // Label for the subaddress.
		NumUnspentOutputs uint64 `json:"num_unspent_outputs"` // Number of unspent outputs available for the subaddress.
		BlocksToUnlock    int64  `json:"blocks_to_unlock"`    // Blocks to unlock
	} `json:"per_subaddress"` // Array of subaddress information. Balance information for each subaddress in an account:
}

// RequestGetAddress used by GetAddress, return the wallet's addresses for an account. Optionally filter for specific set of subaddresses.
type RequestGetAddress struct {
	AccountIndex uint64   `json:"account_index"` // Return subaddresses for this account.
	AddressIndex []uint64 `json:"address_index"` // (Optional) List of subaddresses to return from an account.
}

// ResponseGetAddress response of GetAddress
type ResponseGetAddress struct {
	Address   string `json:"address"` // The 95-character hex address string of the monero-wallet-rpc in session.
	Addresses []struct {
		Address      string `json:"address"`       // The 95-character hex (sub)address string.
		Label        string `json:"label"`         // Label of the (sub)address
		AddressIndex uint64 `json:"address_index"` // Index of the subaddress
		Used         bool   `json:"used"`          // States if the (sub)address has already received funds
	} `json:"addresses"` // Array of addresses information
}

// RequestGetAddressIndex used by GetAddressIndex , Get account and address indexes from a specific (sub)address
type RequestGetAddressIndex struct {
	Address string `json:"address"` // (Sub)address to look for.
}

// ResponseGetAddressIndex response of GetAddressIndex
type ResponseGetAddressIndex struct {
	Index struct {
		Major uint64 `json:"major"` // Major - Account index.
		Minor uint64 `json:"minor"` // Minor - Address index.
	} `json:"index"` // Index Subaddress information
}

// RequestCreateAddress used by CreateAddress
// Create a new address for an account. Optionally, label the new address.
type RequestCreateAddress struct {
	AccountIndex uint64 `json:"account_index"` // Create a new address for this account.
	Label        string `json:"label"`         // (Optional) Label for the new address.
}

// ResponseCreateAddress response of CreateAddress
type ResponseCreateAddress struct {
	Address      string `json:"address"`       // Newly created address. Base58 representation of the public keys.
	AddressIndex uint64 `json:"address_index"` // Index of the new address under the input account.
}

// RequestLabelAddress  request of LabelAddress (label_address)
// Label an address.
type RequestLabelAddress struct {
	Index struct {
		Major uint64 `json:"major"` // Account index for the subaddress.
		Minor uint64 `json:"minor"` // Index of the subaddress in the account.
	} `json:"index"` // Subaddress index; JSON Object containing the major & minor address
	Label string `json:"label"` // Label for the address.
}

// RequestValidateAddress request of ValidateAddress (validate_address)
// Analyzes a string to determine whether it is a valid monero wallet address and returns the result and the address specifications.
type RequestValidateAddress struct {
	Address        string `json:"address"`                   // The address to validate.
	AnyNetType     bool   `json:"any_net_type,omitempty"`    // (Optional); If true, consider addresses belonging to any of the three Monero networks (mainnet, stagenet, and testnet) valid. Otherwise, only consider an address valid if it belongs to the network on which the rpc-wallet's current daemon is running (Defaults to false).
	AllowOpenAlias bool   `json:"allow_openalias,omitempty"` // (Optional); If true, consider OpenAlias-formatted addresses valid (Defaults to false).
}

// ResponseValidateAddress response of ValidateAddress
type ResponseValidateAddress struct {
	Valid            bool   `json:"valid"`             // True if the input address is a valid Monero address.
	Integrated       bool   `json:"integrated"`        // True if the given address is an integrated address.
	Subaddress       bool   `json:"subaddress"`        // True if the given address is a subaddress
	NetType          string `json:"nettype"`           // Specifies which of the three Monero networks (mainnet, stagenet, and testnet) the address belongs to.
	OpenAliasAddress string `json:"openalias_address"` // True if the address is OpenAlias-formatted.
}

// RequestGetAccounts request of GetAccounts (get_accounts)
// Get all accounts for a wallet. Optionally filter accounts by tag.
type RequestGetAccounts struct {
	Tag string `json:"tag"` // (Optional) Tag for filtering accounts.
}

// ResponseGetAccounts response of GetAccounts
type ResponseGetAccounts struct {
	SubaddressAccounts []struct {
		AccountIndex    uint64 `json:"account_index"`    // Index of the account.
		Balance         uint64 `json:"balance"`          // Balance of the account (locked or unlocked).
		BaseAddress     string `json:"base_address"`     // Base64 representation of the first subaddress in the account.
		Label           string `json:"label"`            // (Optional) Label of the account.
		Tag             string `json:"tag"`              // (Optional) Tag for filtering accounts.
		UnlockedBalance uint64 `json:"unlocked_balance"` // Unlocked balance for the account.
	} `json:"subaddress_accounts"` // Array of subaddress account information:
	TotalBalance         uint64 `json:"total_balance"`          // Total balance of the selected accounts (locked or unlocked).
	TotalUnlockedBalance uint64 `json:"total_unlocked_balance"` // Total unlocked balance of the selected accounts.
}

// RequestCreateAccount request of CreateAccount (create_account)
// Create a new account with an optional label.
type RequestCreateAccount struct {
	// (Optional) Label for the account.
	Label string `json:"label"`
}

// ResponseCreateAccount response of CreateAccount
type ResponseCreateAccount struct {
	AccountIndex uint64 `json:"account_index"` // Index of the new account.
	Address      string `json:"address"`       // Address for this account. Base58 representation of the public keys.
}

// RequestLabelAccount request of LabelAccount (label_account)
type RequestLabelAccount struct {
	AccountIndex uint64 `json:"account_index"` // Apply label to account at this index.
	Label        string `json:"label"`         // Label for the account.
}

// ResponseGetAccountTags response of  GetAccountTags (get_account_tags)
type ResponseGetAccountTags struct {
	AccountTags []struct {
		Tag      string   `json:"tag"`      // Filter tag.
		Label    string   `json:"label"`    // Label for the tag.
		Accounts []uint64 `json:"accounts"` // List of tagged account indices.
	} `json:"account_tags"` // Array of account tag information
}

// RequestTagAccounts request of TagAccounts (tag_accounts)
// Apply a filtering tag to a list of accounts.
type RequestTagAccounts struct {
	Tag      string   `json:"tag"`      // Tag for the accounts.
	Accounts []uint64 `json:"accounts"` // Tag this list of accounts.
}

// RequestUntagAccounts request of UntagAccounts (untag_accounts)
// Remove filtering tag from a list of accounts.
type RequestUntagAccounts struct {
	Accounts []uint64 `json:"accounts"` // Remove tag from this list of accounts.
}

// RequestSetAccountTagDescription request of SetAccountTagDescription (set_account_tag_description)
// Set description for an account tag
type RequestSetAccountTagDescription struct {
	Tag         string `json:"tag"`         // Set a description for this tag.
	Description string `json:"description"` // Description for the tag.
}

// ResponseGetHeight response of GetHeight
// Returns the wallet's current block height.
type ResponseGetHeight struct {
	Height uint64 `json:"height"` // The current monero-wallet-rpc's blockchain height. If the wallet has been offline for a long time, it may need to catch up with the daemon.
}

// RequestTransfer request of Transfer (transfer)
// Send monero to a number of recipients.
type RequestTransfer struct {
	Destinations   []*Destination `json:"destinations"`              // Array of destinations to receive XMR
	AccountIndex   uint64         `json:"account_index"`             // (Optional) Transfer from this account index. (Defaults to 0)
	SubaddrIndices []uint64       `json:"subaddr_indices"`           // (Optional) Transfer from this set of subaddresses. (Defaults to empty - all indices)
	Priority       Priority       `json:"priority"`                  // Set a priority for the transaction. Accepted Values are: 0-3 for: default, unimportant, normal, elevated, priority.
	Mixing         uint64         `json:"mixin"`                     // Number of outputs from the blockchain to mix with (0 means no mixing).
	RingSize       uint64         `json:"ring_size,omitempty"`       // (Optional) Number of outputs to mix in the transaction (this output + N decoys from the blockchain).
	UnlockTime     uint64         `json:"unlock_time"`               // Number of blocks before the monero can be spent (0 to not add a lock).
	PaymentID      string         `json:"payment_id"`                // (Optional) Random 32-byte/64-character hex string to identify a transaction.
	GetTxKey       bool           `json:"get_tx_key"`                // (Optional) Return the transaction key after sending.
	DoNotRelay     bool           `json:"do_not_relay,omitempty"`    // (Optional) If true, the newly created transaction will not be relayed to the monero network. (Defaults to false)
	GetTxHex       bool           `json:"get_tx_hex,omitempty"`      // (Optional) Return the transaction as hex string after sending (Defaults to false)
	GetTxMetadata  bool           `json:"get_tx_metadata,omitempty"` // (Optional) Return the metadata needed to relay the transaction. (Defaults to false)
}

// ResponseTransfer response of Transfer
type ResponseTransfer struct {
	Amount        uint64 `json:"amount"`         // Amount transferred for the transaction.
	Fee           uint64 `json:"fee"`            // Integer value of the fee charged for the txn.
	MultisigTxset string `json:"multisig_txset"` // MultiTxSet multisig_txset - Set of multisig transactions in the process of being signed (empty for non-multisig).
	TxBlob        string `json:"tx_blob"`        // Raw transaction represented as hex string, if get_tx_hex is true.
	TxHash        string `json:"tx_hash"`        // String for the publically searchable transaction hash.
	TxKey         string `json:"tx_key"`         // String for the transaction key if get_tx_key is true, otherwise, blank string.
	TxMetadata    string `json:"tx_metadata"`    // Set of transaction metadata needed to relay this transfer later, if get_tx_metadata is true.
	UnsignedTxSet string `json:"unsigned_txset"` // Set of unsigned tx for cold-signing purposes.
}

// RequestTransferSplit request of TransferSplit (transfer_split)
// Same as transfer, but can split into more than one tx if necessary
type RequestTransferSplit struct {
	Destinations   []*Destination `json:"destinations"`              // Array of destinations to receive XMR
	AccountIndex   uint64         `json:"account_index"`             // (Optional) Transfer from this account index. (Defaults to 0)
	SubaddrIndices []uint64       `json:"subaddr_indices"`           // (Optional) Transfer from this set of subaddresses. (Defaults to empty - all indices)
	Mixin          uint64         `json:"mixin"`                     // Number of outputs from the blockchain to mix with (0 means no mixing).
	RingSize       uint64         `json:"ring_size,omitempty"`       // (Optional) Sets ringsize to n (mixin + 1).
	UnlockTime     uint64         `json:"unlock_time"`               // Number of blocks before the monero can be spent (0 to not add a lock).
	PaymendID      string         `json:"payment_id"`                // (Optional) Random 32-byte/64-character hex string to identify a transaction.
	GetxKeys       bool           `json:"get_tx_keys"`               // (Optional) Return the transaction keys after sending.
	Priority       Priority       `json:"priority"`                  // Set a priority for the transactions. Accepted Values are: 0-3 for: default, unimportant, normal, elevated, priority.
	DoNotRelay     bool           `json:"do_not_relay,omitempty"`    // (Optional) If true, the newly created transaction will not be relayed to the monero network. (Defaults to false)
	GetTxHex       bool           `json:"get_tx_hex,omitempty"`      // (Optional) Return the transactions as hex string after sending
	NewAlgorithm   bool           `json:"new_algorithm"`             // True to use the new transaction construction algorithm, defaults to false.
	GetTxMetadata  bool           `json:"get_tx_metadata,omitempty"` // (Optional) Return list of transaction metadata needed to relay the transfer later.
}

// ResponseTransferSplit response of TransferSplit
type ResponseTransferSplit struct {
	TxHashList     []string `json:"tx_hash_list"`     // The tx hashes of every transaction.
	TxKeyList      []string `json:"tx_key_list"`      // The transaction keys for every transaction.
	AmountList     []uint64 `json:"amount_list"`      // The amount transferred for every transaction.
	FeeList        []uint64 `json:"fee_list"`         // The amount of fees paid for every transaction.
	TxBlobList     []string `json:"tx_blob_list"`     // The tx as hex string for every transaction.
	TxMetadataList []string `json:"tx_metadata_list"` // List of transaction metadata needed to relay the transactions later.
	MultisigTxSet  string   `json:"multisig_txset"`   // The set of signing keys used in a multisig transaction (empty for non-multisig).
	UnsignedTxSet  string   `json:"unsigned_txset"`   // Set of unsigned tx for cold-signing purposes.
}

// RequestSignTransfer request of SignTransfer (sign_transfer)
// Sign a transaction created on a read-only wallet (in cold-signing process)
type RequestSignTransfer struct {
	UnsighnedxSet string `json:"unsigned_txset"`       // Set of unsigned tx returned by "transfer" or "transfer_split" methods.
	ExportRaw     bool   `json:"export_raw,omitempty"` // (Optional) If true, return the raw transaction data. (Defaults to false)
}

// ResponseSignTransfer response of SignTransfer
type ResponseSignTransfer struct {
	SignedTxSet string   `json:"signed_txset"` // Set of signed tx to be used for submitting transfer.
	TxHashList  []string `json:"tx_hash_list"` // The tx hashes of every transaction.
	TxRawList   []string `json:"tx_raw_list"`  // The tx raw data of every transaction.
}

// RequestSubmitTransfer request of SubmitTransfer (submit_transfer)
// Submit a previously signed transaction on a read-only wallet (in cold-signing process)
type RequestSubmitTransfer struct {
	TxDataHex string `json:"tx_data_hex"` // Set of signed tx returned by "sign_transfer"
}

// ResponseSubmitTransfer response of SubmitTransfer
type ResponseSubmitTransfer struct {
	TxHashList []string `json:"tx_hash_list"` // The tx hashes of every transaction.
}

// RequestSweepDust request of SweepDust (sweep_dust)
// Send all dust outputs back to the wallet's, to make them easier to spend (and mix).
type RequestSweepDust struct {
	GetTxKeys     bool `json:"get_tx_keys"`               // (Optional) Return the transaction keys after sending.
	DoNotRelay    bool `json:"do_not_relay,omitempty"`    // (Optional) If true, the newly created transaction will not be relayed to the monero network. (Defaults to false)
	GetTxHey      bool `json:"get_tx_hex,omitempty"`      // (Optional) Return the transactions as hex string after sending. (Defaults to false)
	GetTxMetadata bool `json:"get_tx_metadata,omitempty"` // (Optional) Return list of transaction metadata needed to relay the transfer later. (Defaults to false)
}

// ResponseSweepDust response of SweepDust
type ResponseSweepDust struct {
	TxHashList     []string `json:"tx_hash_list"`     // The tx hashes of every transaction.
	TxKeyList      []string `json:"tx_key_list"`      // The transaction keys for every transaction.
	AmountList     []uint64 `json:"amount_list"`      //  The amount transferred for every transaction.
	FeeList        []uint64 `json:"fee_list"`         //  The amount of fees paid for every transaction.
	TxBlobList     []string `json:"tx_blob_list"`     // The tx as hex string for every transaction.
	TxMetadataList []string `json:"tx_metadata_list"` // List of transaction metadata needed to relay the transactions later.
	MultisigTxSet  string   `json:"multisig_txset"`   // The set of signing keys used in a multisig transaction (empty for non-multisig).
	UnsignedTxSet  string   `json:"unsigned_txset"`   // Set of unsigned tx for cold-signing purposes.
}

// RequestSweepAll request of SweepAll (sweep_all)
// Send all unlocked balance to an address.
type RequestSweepAll struct {
	Address           string   `json:"address"`                   //  Destination public address.
	AccountIndex      uint64   `json:"account_index"`             //  Sweep transactions from this account.
	SubaddrIndices    []uint64 `json:"subaddr_indices"`           //  (Optional) Sweep from this set of subaddresses in the account.
	SubaddrIndicesAll bool     `json:"subaddr_indices_all"`       //  (Optional) Sweep from all subaddresses in the account (default: false).
	Priority          Priority `json:"priority"`                  //  (Optional) Priority for sending the sweep transfer, partially determines fee.
	Mixin             uint64   `json:"mixin"`                     //  Number of outputs from the blockchain to mix with (0 means no mixing).
	RingSize          uint64   `json:"ring_size,omitempty"`       //  (Optional) Sets ringsize to n (mixin + 1).
	UnlockTime        uint64   `json:"unlock_time"`               //  Number of blocks before the monero can be spent (0 to not add a lock).
	PaymentID         string   `json:"payment_id"`                //  (Optional) Random 32-byte/64-character hex string to identify a transaction.
	GetTxKeys         bool     `json:"get_tx_keys"`               //  (Optional) Return the transaction keys after sending.
	BelowAmount       uint64   `json:"below_amount"`              //  (Optional) Include outputs below this amount.
	DoNotRelay        bool     `json:"do_not_relay,omitempty"`    //  (Optional) If true, do not relay this sweep transfer. (Defaults to false)
	GetTxHex          bool     `json:"get_tx_hex,omitempty"`      //  (Optional) return the transactions as hex encoded string. (Defaults to false)
	GetTxMetadata     bool     `json:"get_tx_metadata,omitempty"` //  (Optional) return the transaction metadata as a string. (Defaults to false)
}

// ResponseSweepAll response of SweepAll
type ResponseSweepAll struct {
	TxHashList     []string `json:"tx_hash_list"`     // The tx hashes of every transaction.
	TxKeyList      []string `json:"tx_key_list"`      // The transaction keys for every transaction.
	AmountList     []uint64 `json:"amount_list"`      // The amount transferred for every transaction.
	FeeList        []uint64 `json:"fee_list"`         // The amount of fees paid for every transaction.
	TxBlobList     []string `json:"tx_blob_list"`     // The tx as hex string for every transaction.
	TxMetadataList []string `json:"tx_metadata_list"` // List of transaction metadata needed to relay the transactions later.
	MultisigTxSet  string   `json:"multisig_txset"`   // Set of signing keys used in a multisig transaction (empty for non-multisig).
	UnsignedTxSet  string   `json:"unsigned_txset"`   // Set of unsigned tx for cold-signing purposes.s
}

// RequestSweepSingle request of SweepSingle (sweep_single)
// Send all of a specific unlocked output to an address
type RequestSweepSingle struct {
	Address        string   `json:"address"`                   // Destination public address
	AccountIndex   uint64   `json:"account_index"`             // Sweep transactions from this account.
	SubaddrIndices []uint64 `json:"subaddr_indices"`           // (Optional) Sweep from this set of subaddresses in the account.s
	Priority       Priority `json:"priority"`                  // (Optional) Priority for sending the sweep transfer, partially determines fee.
	Mixin          uint64   `json:"mixin"`                     // Number of outputs from the blockchain to mix with (0 means no mixing).
	RingSize       uint64   `json:"ring_size,omitempty"`       // (Optional) Sets ringsize to n (mixin + 1).
	UnlockTime     uint64   `json:"unlock_time"`               // Number of blocks before the monero can be spent (0 to not add a lock).
	PaymentID      string   `json:"payment_id"`                // (Optional) Random 32-byte/64-character hex string to identify a transaction.
	GetxKeys       bool     `json:"get_tx_keys"`               // (Optional) Return the transaction keys after sending.
	KeyImage       string   `json:"key_image"`                 // Key image of specific output to sweep.
	BelowAmount    uint64   `json:"below_amount"`              // (Optional) Include outputs below this amount.
	DoNotRelay     bool     `json:"do_not_relay,omitempty"`    // (Optional) If true, do not relay this sweep transfer. (Defaults to false)
	GetTxHey       bool     `json:"get_tx_hex,omitempty"`      // (Optional) return the transactions as hex encoded string. (Defaults to false)
	GetTxMetadata  bool     `json:"get_tx_metadata,omitempty"` // (Optional) return the transaction metadata as a string. (Defaults to false)
}

// ResponseSweepSingle response of SweepSingle
type ResponseSweepSingle struct {
	TxHashList     []string `json:"tx_hash_list"`     // The tx hashes of every transaction.
	TxKeyList      []string `json:"tx_key_list"`      // The transaction keys for every transaction.
	AmountList     []uint64 `json:"amount_list"`      // The amount transferred for every transaction.
	FreeList       []uint64 `json:"fee_list"`         // The amount of fees paid for every transaction.
	TxBlobList     []string `json:"tx_blob_list"`     // The tx as hex string for every transaction.
	TxMetadataList []string `json:"tx_metadata_list"` // List of transaction metadata needed to relay the transactions later.
	MultisigTxSet  string   `json:"multisig_txset"`   // The set of signing keys used in a multisig transaction (empty for non-multisig).
	UnsignedTxSet  string   `json:"unsigned_txset"`   // Set of unsigned tx for cold-signing purposes.
}

// RequestRelayTx request of RelayTx (relay_tx)
// Relay a transaction previously created with "do_not_relay":true.
type RequestRelayTx struct {
	Hex string `json:"hex"` // Transaction metadata returned from a transfer method with get_tx_metadata set to true.
}

// ResponseRelayTx response of RelayTx
type ResponseRelayTx struct {
	TxHash string `json:"tx_hash"` // String for the publically searchable transaction hash.
}

// RequestGetPayments request of GetPayments (get_payments)
// Get a list of incoming payments using a given payment id.
type RequestGetPayments struct {
	PaymentID string `json:"payment_id"` // Payment ID used to find the payments (16 characters hex).
}

// ResponseGetPayments response of GetPayments
type ResponseGetPayments struct {
	Payments []struct {
		PaymentID    string `json:"payment_id"`   // Payment ID matching the input parameter.
		TxHash       string `json:"tx_hash"`      // Transaction hash used as the transaction ID.
		Amount       uint64 `json:"amount"`       // Amount for this payment.
		BlockHeight  uint64 `json:"block_height"` // Height of the block that first confirmed this payment.
		UnlockTime   uint64 `json:"unlock_time"`  // Time (in block height) until this payment is safe to spend.
		SubaddrIndex struct {
			Major uint64 `json:"major"` // Account index for the subaddress.
			Minor uint64 `json:"minor"` // Index of the subaddress in the account.
		} `json:"subaddr_index"` // Subaddress index
		Address string `json:"address"` // Address receiving the payment; Base58 representation of the public keys.
	} `json:"payments"` // list of payments
}

// RequestGetBulkPayments request of GetBulkPayments (get_bulk_payments)
// Get a list of incoming payments using a given payment id, or a list of payments ids, from a given height.
// This method is the preferred method over get_payments because it has the same functionality but is more extendable.
// Either is fine for looking up transactions by a single payment ID.
type RequestGetBulkPayments struct {
	PaymentIDs     []string `json:"payment_ids"`      // Payment IDs used to find the payments (16 characters hex).
	MinBlockHeight uint64   `json:"min_block_height"` // The block height at which to start looking for payments.
}

// ResponseGetBulkPayments response of GetBulkPayments
type ResponseGetBulkPayments struct {
	Payments []struct {
		PaymentID    string `json:"payment_id"`   // Payment ID matching one of the input IDs.
		TxHash       string `json:"tx_hash"`      // Transaction hash used as the transaction ID.
		Amount       uint64 `json:"amount"`       // Amount for this payment.
		BlockHeight  uint64 `json:"block_height"` // Height of the block that first confirmed this payment.
		UnlockTime   uint64 `json:"unlock_time"`  // Time (in block height) until this payment is safe to spend.
		SubaddrIndex struct {
			Major uint64 `json:"major"` // Account index for the subaddress.
			Minor uint64 `json:"minor"` // Index of the subaddress in the account.
		} `json:"subaddr_index"` // Subaddress index:
		Address string `json:"address"` // Address receiving the payment; Base58 representation of the public keys.
	} `json:"payments"` // List of payments
}

// RequestIncomingTransfers request of IncomingTransfers (incoming_transfers)
// Return a list of incoming transfers to the wallet.
type RequestIncomingTransfers struct {
	TransferType   string   `json:"transfer_type"`   // "all": all the transfers, "available": only transfers which are not yet spent, OR "unavailable": only transfers which are already spent.
	AccountIndex   uint64   `json:"account_index"`   // (Optional) Return transfers for this account. (defaults to 0)
	SubaddrIndices []uint64 `json:"subaddr_indices"` // (Optional) Return transfers sent to these subaddresses.
	Verbose        bool     `json:"verbose"`         // (Optional) Enable verbose output, return key image if true.
}

// ResponseIncomingTransfers response of IncomingTransfers
type ResponseIncomingTransfers struct {
	Transfers []struct {
		Amount       uint64 `json:"amount"`       // Amount of this transfer.
		GlobalIndex  uint64 `json:"global_index"` // Mostly internal use, can be ignored by most users.
		KeyImage     string `json:"key_image"`    // Key image for the incoming transfer's unspent output (empty unless verbose is true).
		Spent        bool   `json:"spent"`        // Indicates if this transfer has been spent.
		SubaddrIndex struct {
			Major uint64 `json:"major"` // Account index for the subaddress.
			Minor uint64 `json:"minor"` // Index of the subaddress in the account.
		} `json:"subaddr_index"`
		TxHash string `json:"tx_hash"` // Several incoming transfers may share the same hash if they were in the same transaction.
		TxSize uint64 `json:"tx_size"` // Size of transaction in bytes.
	} `json:"transfers"` // list of transfers:
}

// RequestQueryKey request of QueryKey (query_key)
// Return the spend or view private key.
type RequestQueryKey struct {
	KeyType string `json:"key_type"` // Which key to retrieve: "mnemonic" - the mnemonic seed (older wallets do not have one) OR "view_key" - the view key
}

// ResponseQueryKey response of QueryKey
type ResponseQueryKey struct {
	Key string `json:"key"` // The view key will be hex encoded, while the mnemonic will be a string of words.
}

// RequestMakeIntegratedAddress request of MakeIntegratedAddress (make_integrated_address)
// Make an integrated address from the wallet address and a payment id.
type RequestMakeIntegratedAddress struct {
	StandardAddress string `json:"standard_address"` // (Optional, defaults to primary address) Destination public address.
	PaymentID       string `json:"payment_id"`       // (Optional, defaults to a random ID) 16 characters hex encoded.
}

// ResponseMakeIntegratedAddress response of MakeIntegratedAddress
type ResponseMakeIntegratedAddress struct {
	IntegratedAddress string `json:"integrated_address"` // The newly created integrated address
	PaymentID         string `json:"payment_id"`         // Hex encoded payment id
}

// RequestSplitIntegratedAddress request of SplitIntegratedAddress (split_integrated_address)
// Retrieve the standard address and payment id corresponding to an integrated address.
type RequestSplitIntegratedAddress struct {
	IntegratedAddress string `json:"integrated_address"` // Integrated address
}

// ResponseSplitIntegratedAddress response of SplitIntegratedAddress
type ResponseSplitIntegratedAddress struct {
	IsSubaddress    bool   `json:"is_subaddress"`    // States if the address is a subaddress
	PaymentID       string `json:"payment_id"`       // Hex encoded payment id
	StandardAddress string `json:"standard_address"` // Address of integrated address
}

// RequestSetTxNotes request of SetTxNotes (set_tx_notes)
// Set arbitrary string notes for transactions.
type RequestSetTxNotes struct {
	TxIDs []string `json:"txids"` // Transaction ids
	Notes []string `json:"notes"` // Notes for the transactions
}

// RequestGetTxNotes request of GetTxNotes (get_tx_notes)
type RequestGetTxNotes struct {
	TxIDs []string `json:"txids"` // Transaction ids
}

// ResponseGetTxNotes response of GetTxNotes
type ResponseGetTxNotes struct {
	Notes []string `json:"notes"` // Notes for the transactions
}

// RequestSetAttribute request of SetAttribute (set_attribute)
// Set arbitrary attribute.
type RequestSetAttribute struct {
	Key   string `json:"key"`   // Attribute name
	Value string `json:"value"` // Attribute value
}

// RequestGetAttribute request of GetAttribute (get_attribute)
type RequestGetAttribute struct {
	Key string `json:"key"` // Attribute name
}

// ResponseGetAttribute response of GetAttribute
type ResponseGetAttribute struct {
	Value string `json:"value"` // Attribute value
}

// RequestGetTxKey request of GetTxKey (get_tx_key)
// Get transaction secret key from transaction id
type RequestGetTxKey struct {
	TxID string `json:"txid"` // Transaction id
}

// ResponseGetTxKey response of GetTxKey
type ResponseGetTxKey struct {
	TxKey string `json:"tx_key"` // Transaction secret key
}

// RequestCheckTxKey request of CheckTxKey (check_tx_key)
// Check a transaction in the blockchain with its secret key
type RequestCheckTxKey struct {
	TxID    string `json:"txid"`    // Transaction id.
	TxKey   string `json:"tx_key"`  // Transaction secret key.
	Address string `json:"address"` // Destination public address of the transaction.
}

// ResponseCheckTxKey response of CheckTxKey
type ResponseCheckTxKey struct {
	Confirmations uint64 `json:"confirmations"` // Number of block mined after the one with the transaction.
	InPool        bool   `json:"in_pool"`       // States if the transaction is still in pool or has been added to a block.s
	Received      uint64 `json:"received"`      // Amount of the transaction.
}

// RequestGetTxProof request of GetTxProof (get_tx_proof)
// Get transaction signature to prove it
type RequestGetTxProof struct {
	TxID    string `json:"txid"`    // Transaction id
	Address string `json:"address"` // Destination public address of the transaction.
	Message string `json:"message"` // (Optional) add a message to the signature to further authenticate the proving process.
}

// ResponseGetTxProof response of GetTxProof
type ResponseGetTxProof struct {
	Signature string `json:"signature"` // Transaction signature.
}

// RequestCheckTxProof request of CheckTxProof (check_tx_proof)
// Prove a transaction by checking its signature.
type RequestCheckTxProof struct {
	TxID      string `json:"txid"`      // Transaction id.
	Address   string `json:"address"`   // Destination public address of the transaction.
	Message   string `json:"message"`   // (Optional) Should be the same message used in get_tx_proof.
	Signature string `json:"signature"` // Transaction signature to confirm.
}

// ResponseCheckTxProof response of CheckTxProof
type ResponseCheckTxProof struct {
	Confirmations uint64 `json:"confirmations"` // Number of block mined after the one with the transaction.
	Good          bool   `json:"good"`          // States if the inputs proves the transaction.
	InPool        bool   `json:"in_pool"`       // States if the transaction is still in pool or has been added to a block.
	Received      uint64 `json:"received"`      // Amount of the transaction.
}

// RequestGetSpendProof request of GetSpendProof (get_spend_proof)
// Generate a signature to prove a spend. Unlike proving a transaction, it does not requires the destination public address.
type RequestGetSpendProof struct {
	TxID    string `json:"txid"`    // Transaction id.
	Message string `json:"message"` // (Optional) add a message to the signature to further authenticate the prooving process.
}

// ResponseGetSpendProof response of GetSpendProof
type ResponseGetSpendProof struct {
	Signature string `json:"signature"` // Spend signature.
}

// RequestCheckSpendProof request of CheckSpendProof (check_spend_proof)
type RequestCheckSpendProof struct {
	TxID      string `json:"txid"`      // Transaction id.
	Message   string `json:"message"`   // (Optional) Should be the same message used in get_spend_proof.
	Signature string `json:"signature"` // Spend signature to confirm.
}

// ResponseCheckSpendProof response of CheckSpendProof
type ResponseCheckSpendProof struct {
	Good bool `json:"good"` // States if the inputs proves the spend.
}

// RequestGetReserveProof request of GetReserveProof (get_reserve_proof)
// Generate a signature to prove of an available amount in a wallet
type RequestGetReserveProof struct {
	All          bool   `json:"all"`           // Proves all wallet balance to be disposable.
	AccountIndex uint64 `json:"account_index"` // Specify the account from witch to prove reserve. (ignored if all is set to true)
	Amount       uint64 `json:"amount"`        // Amount (in atomic units) to prove the account has for reserve. (ignored if all is set to true)
	Message      string `json:"message"`       // (Optional) add a message to the signature to further authenticate the proving process. If a message is added to get_reserve_proof (optional), this message will be required when using check_reserve_proof
}

// ResponseGetReserveProof response of GetReserveProof
type ResponseGetReserveProof struct {
	Signature string `json:"signature"` // Reserve signature
}

// RequestCheckReserveProof request of CheckReserveProof (check_reserve_proof)
// Proves a wallet has a disposable reserve using a signature
type RequestCheckReserveProof struct {
	Address   string `json:"address"`   // Public address of the wallet.
	Message   string `json:"message"`   // (Optional) Should be the same message used in get_reserve_proof.
	Signature string `json:"signature"` // Reserve signature to confirm.
}

// ResponseCheckReserveProof response of CheckReserveProof
type ResponseCheckReserveProof struct {
	Good bool `json:"good"` // States if the inputs proves the reserve.
}

// RequestGetTransfers request of GetTransfers (get_transfers)
// Returns a list of transfers.
type RequestGetTransfers struct {
	In             bool     `json:"in"`                   // (Optional) Include incoming transfers.
	Out            bool     `json:"out"`                  // (Optional) Include outgoing transfers.
	Pending        bool     `json:"pending"`              // (Optional) Include pending transfers.
	Failed         bool     `json:"failed"`               // (Optional) Include failed transfers.
	Pool           bool     `json:"pool"`                 // (Optional) Include transfers from the daemon's transaction pool.
	FilterByHeight bool     `json:"filter_by_height"`     // (Optional) Filter transfers by block height.
	MinHeight      uint64   `json:"min_height"`           // (Optional) Minimum block height to scan for transfers, if filtering by height is enabled.
	MaxHeight      uint64   `json:"max_height,omitempty"` // (Opional) Maximum block height to scan for transfers, if filtering by height is enabled (defaults to max block height).
	AccountIndex   uint64   `json:"account_index"`        // (Optional) Index of the account to query for transfers. (defaults to 0)
	SubaddrIndices []uint64 `json:"subaddr_indices"`      // (Optional) List of subaddress indices to query for transfers. (Defaults to empty - all indices)
}

// Transfer information
type Transfer struct {
	Address         string         `json:"address"`           // Public address of the transfer.
	Amount          uint64         `json:"amount"`            // Amount transferred.
	Confirmations   uint64         `json:"confirmations"`     // Number of block mined since the block containing this transaction (or block height at which the transaction should be added to a block if not yet confirmed).
	Destinations    []*Destination `json:"destinations"`      // JSON objects containing transfer destinations:
	DoubleSpendSeen bool           `json:"double_spend_seen"` // True if the key image(s) for the transfer have been seen before.
	Fee             uint64         `json:"fee"`               // Transaction fee for this transfer.
	Height          uint64         `json:"height"`            // Height of the first block that confirmed this transfer (0 if not mined yet).
	Note            string         `json:"note"`              // Note about this transfer.
	PaymentID       string         `json:"payment_id"`        // Payment ID for this transfer.
	SubaddrIndex    struct {
		Major uint64 `json:"major"` // Account index for the subaddress.
		Minor uint64 `json:"minor"` // Index of the subaddress under the account.
	} `json:"subaddr_index"` // JSON object containing the major & minor subaddress index:
	SuggestedConfirmationsThreshold uint64 `json:"suggested_confirmations_threshold"` // Estimation of the confirmations needed for the transaction to be included in a block.
	Timestamp                       uint64 `json:"timestamp"`                         // POSIX timestamp for when this transfer was first confirmed in a block (or timestamp submission if not mined yet).
	TxID                            string `json:"txid"`                              // Transaction ID for this transfer.
	Type                            string `json:"type"`                              // Transfer type: "in/out/pending/failed/pool"
	UnlockTime                      uint64 `json:"unlock_time"`                       // Number of blocks until transfer is safely spendable.
}

// ResponseGetTransfers response of GetTransfers
type ResponseGetTransfers struct {
	In      []*Transfer `json:"in"`
	Out     []*Transfer `json:"out"`
	Pending []*Transfer `json:"pending"`
	Failed  []*Transfer `json:"failed"`
	Pool    []*Transfer `json:"pool"`
}

// RequestGetTransferByTxID request of GetTransferByTxID (get_transfer_by_txid)
// Show information about a transfer to/from this address.
type RequestGetTransferByTxID struct {
	TxID         string `json:"txid"`                    // Transaction ID used to find the transfer.
	AccountIndex uint64 `json:"account_index,omitempty"` // (Optional) Index of the account to query for the transfer.
}

// ResponseGetTransferByTxID response of GetTransferByTxID
type ResponseGetTransferByTxID struct {
	Transfer Transfer `json:"transfer"` // JSON object containing payment information:
}

// RequestSign request of Sign (sign)
// Sign a string
type RequestSign struct {
	Data string `json:"data"` // Anything you need to sign.
}

// ResponseSign response of Sign
type ResponseSign struct {
	Signature string `json:"signature"` // Signature generated against the "data" and the account public address.
}

// RequestVerify request of Verify (verify)
// Verify a signature on a string
type RequestVerify struct {
	Data      string `json:"data"`      // What should have been signed.
	Address   string `json:"address"`   // Public address of the wallet used to sign the data.
	Signature string `json:"signature"` // Signature generated by sign method.
}

// ResponseVerify response of Verify
type ResponseVerify struct {
	Good bool `json:"good"` // True if signature is valid.
}

// ResponseExportOutputs response of ExportOutputs (export_outputs)
// Export outputs in hex format
type ResponseExportOutputs struct {
	OutputsDataHex string `json:"outputs_data_hex"` // Wallet outputs in hex format.
}

// RequestImportOutputs request of ImportOutputs (import_outputs)
// Import outputs in hex format
type RequestImportOutputs struct {
	OutputsDataHex string `json:"outputs_data_hex"` // Wallet outputs in hex format.
}

// ResponseImportOutputs response of ImportOutputs
type ResponseImportOutputs struct {
	NumImported uint64 `json:"num_imported"` // Number of outputs imported.
}

// ResponseExportKeyImages response of ExportKeyImages (export_key_images)
// Export a signed set of key images
type ResponseExportKeyImages struct {
	SignedKeyImages []struct {
		KeyImage  string `json:"key_image"`
		Signature string `json:"signature"`
	} `json:"signed_key_images"` // Array of signed key images:
}

// RequestImportKeyImages request of ImportKeyImages
type RequestImportKeyImages struct {
	SignedKeyImages []*SignedKeyImage `json:"signed_key_images"` // Array of signed key images:
}

// ResponseImportKeyImages response of ImportKeyImages
type ResponseImportKeyImages struct {
	Height  uint64 `json:"height"`
	Spent   uint64 `json:"spent"`   // Amount (in atomic units) spent from those key images.
	Unspent uint64 `json:"unspent"` // Amount (in atomic units) still available from those key images.
}

// RequestMakeURI request of MakeURI (make_uri)
// Create a payment URI using the official URI spec.
type RequestMakeURI struct {
	Address       string `json:"address"`        // Wallet address
	Amount        uint64 `json:"amount"`         // (Optional) the integer amount to receive, in atomic units
	PaymentID     string `json:"payment_id"`     // (Optional) 16 or 64 character hexadecimal payment id
	RecipientName string `json:"recipient_name"` // (Optional) name of the payment recipient
	TxDescription string `json:"tx_description"` // (Optional) Description of the reason for the tx
}

// ResponseMakeURI response of MakeURI
type ResponseMakeURI struct {
	URI string `json:"uri"` // This contains all the payment input information as a properly formatted payment URI
}

// RequestParseURI request of ParseURI (parse_uri)
type RequestParseURI struct {
	URI string `json:"uri"` // This contains all the payment input information as a properly formatted payment URI
}

// ResponseParseURI response of ParseURI
type ResponseParseURI struct {
	URI struct {
		Address       string `json:"address"`        // Wallet address
		Amount        uint64 `json:"amount"`         // Integer amount to receive, in atomic units (0 if not provided)
		PaymentID     string `json:"payment_id"`     // 16 or 64 character hexadecimal payment id (empty if not provided)
		RecipientName string `json:"recipient_name"` // Name of the payment recipient (empty if not provided)
		TxDescription string `json:"tx_description"` // Description of the reason for the tx (empty if not provided)
	} `json:"uri"` // JSON object containing payment information:
}

// RequestGetAddressBook request of GetAddressBook (get_address_book)
type RequestGetAddressBook struct {
	Entries []uint64 `json:"entries"` // Indices of the requested address book entries
}

// ResponseGetAddressBook response of GetAddressBook
type ResponseGetAddressBook struct {
	Entries []struct {
		Address     string `json:"address"`     // Public address of the entry
		Description string `json:"description"` // Description of this address entry
		Index       uint64 `json:"index"`
		PaymentID   string `json:"payment_id"`
	} `json:"entries"` // Array of entries:
}

// RequestAddAddressBook request of AddAddressBook (add_address_book)
// Add an entry to the address book.
type RequestAddAddressBook struct {
	Address     string `json:"address"`
	PaymentID   string `json:"payment_id"`  // (Optional) string, defaults to "0000000000000000000000000000000000000000000000000000000000000000";
	Description string `json:"description"` // (Optional) string, defaults to "";
}

// ResponseAddAddressBook response of AddAddressBook
type ResponseAddAddressBook struct {
	Index uint64 `json:"index"` // The index of the address book entry.
}

// RequestDeleteAddressBook request of DeleteAddressBook (delete_address_book)
// Delete an entry from the address book.
type RequestDeleteAddressBook struct {
	Index uint64 `json:"index"` // The index of the address book entry.
}

// RequestRefresh request of Refresh (refresh)
// Refresh a wallet after opening.
type RequestRefresh struct {
	StartHeight uint64 `json:"start_height,omitempty"` // (Optional) The block height from which to start refreshing.
}

// ResponseRefresh response of Refresh
type ResponseRefresh struct {
	BlocksFetched uint64 `json:"blocks_fetched"` // Number of new blocks scanned.
	ReceivedMoney bool   `json:"received_money"` // States if transactions to the wallet have been found in the blocks.
}

// RequestStartMining request of StartMining (start_mining)
// Start mining in the Monero daemon.
type RequestStartMining struct {
	ThreadsCount       uint64 `json:"threads_count"`        // Number of threads created for mining.
	DoBackgroundMining bool   `json:"do_background_mining"` // Allow to start the miner in smart mining mode.
	IgnoreBattery      bool   `json:"ignore_battery"`       // Ignore battery status (for smart mining only)
}

// ResponseGetLanguages response of GetLanguages (get_languages)
// Get a list of available languages for your wallet's seed
type ResponseGetLanguages struct {
	Languages []string `json:"languages"` // List of available languages
}

// RequestCreateWallet request of CreateWallet (create_wallet)
// Create a new wallet. You need to have set the argument "–wallet-dir" when launching monero-wallet-rpc to make this work
type RequestCreateWallet struct {
	Filename string `json:"filename"` // Wallet file name.
	Password string `json:"password"` // (Optional) password to protect the wallet.
	Language string `json:"language"` // Language for your wallets' seed.
}

// RequestOpenWallet request of OpenWallet (open_wallet)
// Open a wallet. You need to have set the argument "–wallet-dir" when launching monero-wallet-rpc to make this work.
type RequestOpenWallet struct {
	Filename string `json:"filename"` // Wallet name stored in –wallet-dir.
	Password string `json:"password"` // (Optional) only needed if the wallet has a password defined.
}

// RequestSavedPoolWallet request of SavePoolWallet
type RequestSavedPoolWallet struct {
	PoolAddress string `json:"pool_address"` // pool address name
	OldAddress  string `json:"old_address"`  // old wallet address
	Password    string `json:"password"`     // password of the pool wallet
}

// ResponseSavePoolWallet response of SavePoolWallet
type ResponseSavePoolWallet struct {
	SavePoolWalletResponse bool `json:"create_result"`
}

// RequestChangeWalletPassword request of ChangeWalletPassword (change_wallet_password)
type RequestChangeWalletPassword struct {
	OldPassword string `json:"old_password"` // (Optional) Current wallet password, if defined.
	NewPassword string `json:"new_password"` // (Optional) New wallet password, if not blank.
}

// ResponseIsMultisig response of IsMultisig
type ResponseIsMultisig struct {
	Multisig  bool   `json:"multisig"` // States if the wallet is multisig
	Ready     bool   `json:"ready"`
	Threshold uint64 `json:"threshold"` // Amount of signature needed to sign a transfer.
	Total     uint64 `json:"total"`     // Total amount of signature in the multisig wallet.
}

// RequestPrepareMultisig request prepare_multisig
type RequestPrepareMultisig struct {
	EnableMultiSigExperimental bool `json:"enable_multisig_experimental"` // enable multisig
}

// ResponsePrepareMultisig response of PrepareMultisig
type ResponsePrepareMultisig struct {
	MultisigInfo string `json:"multisig_info"` // Multisig string to share with peers to create the multisig wallet.
}

// RequestMakeMultisig request of MakeMultisig(make_multisig)
type RequestMakeMultisig struct {
	MultisigInfo []string `json:"multisig_info"` // List of multisig string from peers.
	Threshold    uint64   `json:"threshold"`     // Amount of signatures needed to sign a transfer. Must be less or equal than the amount of signature in multisig_info.
	Password     string   `json:"password"`      // Wallet password
}

// ResponseMakeMultisig response of MakeMultisig
type ResponseMakeMultisig struct {
	Address      string `json:"address"`       // Multisig wallet address.
	MultisigInfo string `json:"multisig_info"` // Multisig string to share with peers to create the multisig wallet (extra step for N-1/N wallets).
}

// ResponseExportMultisigInfo response of ExportMultisigInfo (export_multisig_info)
// Export multisig info for other participants
type ResponseExportMultisigInfo struct {
	Info string `json:"info"` // Multisig info in hex format for other participants.
}

// RequestImportMultisigInfo request of ImportMultisigInfo(import_multisig_info)
// Import multisig info from other participants
type RequestImportMultisigInfo struct {
	Info []string `json:"info"` // List of multisig info in hex format from other participants.
}

// ResponseImportMultisigInfo response of ImportMultisigInfo
type ResponseImportMultisigInfo struct {
	NOutputs uint64 `json:"n_outputs"` // Number of outputs signed with those multisig info.
}

// RequestExchangeMultisigKeys request of ExchangeMultisig (exchange_multisig)
type RequestExchangeMultisigKeys struct {
	MultisigInfo []string `json:"multisig_info"` // List of multisig string from peers.
	Password     string   `json:"password"`      // Wallet password
}

// ResponseExchangeMultisig response of ExchangeMultisig
type ResponseExchangeMultisig struct {
	MultisigInfo string `json:"multisig_info"` // returned multisig info
	Address      string `json:"address"`       // wallet address
}

// RequestFinalizeMultisig request of FinalizeMultisig (finalize_multisig)
type RequestFinalizeMultisig struct {
	MultisigInfo []string `json:"multisig_info"` // List of multisig string from peers.
	Password     string   `json:"password"`      // Wallet password
}

// ResponseFinalizeMultisig response of FinalizeMultisig
type ResponseFinalizeMultisig struct {
	Address string `json:"address"` // Multisig wallet address.
}

// RequestSignMultisig request of SignMultisig(sign_multisig)
type RequestSignMultisig struct {
	TxDataHex string `json:"tx_data_hex"` // Multisig transaction in hex format, as returned by transfer under multisig_txset.
}

// ResponseSignMultisig response of SignMultisig
type ResponseSignMultisig struct {
	TxDataHex  string   `json:"tx_data_hex"`  // Multisig transaction in hex format.
	TxHashList []string `json:"tx_hash_list"` // List of transaction Hash.
}

// ResponseExportSigPubkey response of ExportSigPubkey
type ResponseExportSigPubkey struct {
	PubKeys []string `json:"multisig_keys"`
}

// RequestSignMultisigParallel request of SignMultisigParallel
type RequestSignMultisigParallel struct {
	TxDataHex string   `json:"tx_data_hex"` // Multisig transaction in hex format, as returned by transfer under multisig_txset.
	PaymentID string   `json:"payment_id"`
	PubKeys   []string `json:"all_signer_pubkeys"`
}

// ResponseSignMultisigParallel response of SignMultisigParallel
type ResponseSignMultisigParallel struct {
	TxDataHex  string   `json:"tx_data_hex"`  // Multisig transaction in hex format.
	TxHashList []string `json:"tx_hash_list"` // List of transaction Hash.
}

// RequestAccuMultisig request of AccuMultisig
type RequestAccuMultisig struct {
	TxDataHex []string `json:"tx_data_hex"` // AccuMultisig transaction in hex format, as returned by transfer under multisig_txset.
}

// ResponseAccuMultisig response of AccuMultisig
type ResponseAccuMultisig struct {
	TxDataHex  string   `json:"tx_data_hex"`  // Multisig transaction in hex format.
	TxHashList []string `json:"tx_hash_list"` // List of transaction Hash.
}

// RequestCheckTransaction request of CheckTransaction
type RequestCheckTransaction struct {
	Destinations []*Destination `json:"destinations"` // Array of destinations to receive XMR:
	TxDataHex    string         `json:"tx_data_hex"`
}

// ResponseCheckTransaction response of CheckTransaction
type ResponseCheckTransaction struct {
	CheckResult bool `json:"check_result"`
}

// RequestSubmitMultisig request of SubmitMultisig (submit_multisig)
// Submit a signed multisig transaction.
type RequestSubmitMultisig struct {
	TxDataHex string `json:"tx_data_hex"` // Multisig transaction in hex format, as returned by sign_multisig under tx_data_hex.
}

// ResponseSubmitMultisig response of SubmitMultisig
type ResponseSubmitMultisig struct {
	TxHashList []string `json:"tx_hash_list"` // List of transaction Hash.
}

// ResponseGetVersion response of GetVersion (get_version)
type ResponseGetVersion struct {
	Version uint64 `json:"version"` // RPC version, formatted with Major * 2^16 + Minor (Major encoded over the first 16 bits, and Minor over the last 16 bits).
}

// RequestRestoreDeterministicWallet request of restore_deterministic_wallet
type RequestRestoreDeterministicWallet struct {
	FileName        string `json:"filename"`          // Name of the wallet.
	Password        string `json:"password"`          // Password of the wallet.
	Seed            string `json:"seed"`              // Mnemonic phrase of the wallet to restore.
	RestoreHeight   int64  `json:"restore_height"`    // (Optional) Block height to restore the wallet from (default = 0).
	Language        string `json:"language"`          // (Optional) Language of the mnemonic phrase in case the old language is invalid.
	SeedOffset      string `json:"seed_offset"`       // (Optional) Offset used to derive a new seed from the given mnemonic to recover a secret wallet from the mnemonic phrase.
	AutoSaveCurrent bool   `json:"auto_save_current"` // Whether to save the currently open RPC wallet before closing it (Defaults to true).
}

// ResponseRestoreDeterministicWallet response of RestoreDeterministicWallet
type ResponseRestoreDeterministicWallet struct {
	Address       string `json:"address"`        // 95-character hexadecimal address of the restored wallet as a string.
	Info          string `json:"info"`           // Message describing the success or failure of the attempt to restore the wallet.
	Seed          string `json:"seed"`           // Mnemonic phrase of the restored wallet, which is updated if the wallet was restored from a deprecated-style mnemonic phrase.
	WasDeprecated bool   `json:"was_deprecated"` // Indicates if the restored wallet was created from a deprecated-style mnemonic phrase.
}
