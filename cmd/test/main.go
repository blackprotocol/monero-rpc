package main

import (
	"encoding/json"
	"fmt"

	rpc "gitlab.com/blackprotocol/monero-rpc"
	"gitlab.com/blackprotocol/monero-rpc/daemon"
)

func main() {
	dc := daemon.New(rpc.Config{
		Address: "http://127.0.0.1:18081/json_rpc",
	})
	resp, err := dc.GetTransactionPool()
	if err != nil {
		panic(err)
	}
	fmt.Println(StringifyObject(resp))
}
func StringifyObject(input any) string {
	result, err := json.MarshalIndent(input, "", "   ")
	if err != nil {
		fmt.Println("fail to json marshal object,err:", err)
	}
	return string(result)
}
