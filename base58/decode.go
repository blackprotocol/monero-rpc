package base58

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"math/big"

	moneroCrypto "gitlab.com/blackprotocol/monero-rpc/crypto"
)

func decodeBlock(dst, src []byte) (int, error) {
	answer := big.NewInt(0)
	j := big.NewInt(1)
	for i := len(src) - 1; i >= 0; i-- {
		tmp := bytes.IndexByte(alphabet, src[i])
		if tmp == -1 {
			if src[i] == 0x00 {
				continue
			}
			return 0, fmt.Errorf("invalid character '%q' found", src[i])
		}
		idx := big.NewInt(int64(tmp))
		tmp1 := big.NewInt(0)
		tmp1.Mul(j, idx)

		answer.Add(answer, tmp1)
		j.Mul(j, big.NewInt(alphabetSize))
	}

	l := blockSizes[len(src)]
	tmp := answer.Bytes()
	copy(dst[l-len(tmp):], tmp)
	return l, nil
}

// DecodeAddress base58 decode the given address , and return raw tag & data
func DecodeAddress(s string) (uint64, []byte, error) {
	b, err := DecodeString(s)
	if err != nil {
		return 0, nil, err
	}

	if len(b) < checksumSize {
		return 0, nil, fmt.Errorf("invalid checksum")
	}

	checksum := b[len(b)-checksumSize:]
	b = b[:len(b)-checksumSize]
	hash := moneroCrypto.NewHash()
	hash.Write(b)
	digest := hash.Sum(nil)
	if !bytes.Equal(checksum, digest[:checksumSize]) {
		return 0, nil, fmt.Errorf("invalid checksum")
	}
	tag, n := binary.Uvarint(b)
	return tag, b[n:], nil
}

type decoder struct {
	err  error
	r    io.Reader
	buf  [fullEncodedBlockSize]byte
	nbuf int
	out  []byte // leftover decoded output
}

func NewDecoder(r io.Reader) io.Reader {
	return &decoder{r: r, out: make([]byte, 0, fullBlockSize)}
}

func (d *decoder) Read(p []byte) (int, error) {
	target := len(p)
	if len(d.out) > 0 {
		d.nbuf = len(d.out)
		copy(p[:target], d.out)
		d.out = make([]byte, fullBlockSize)
	}
	for {
		// Read a block
		n, err := d.r.Read(d.buf[:])
		if n > 0 {
			decodeBuf := make([]byte, fullBlockSize)
			// decode the value read from source
			dn, dErr := decodeBlock(decodeBuf, d.buf[:n])
			if dErr != nil {
				return 0, dErr
			}
			if dn < fullBlockSize {
				decodeBuf = decodeBuf[:dn]
			}
			if d.nbuf+dn <= target {
				copy(p[d.nbuf:], decodeBuf[:dn])
				d.nbuf += dn
			} else {
				remain := target - d.nbuf
				if remain > 0 {
					copy(p[d.nbuf:], decodeBuf[:remain])
				}
				// keep in the buffer
				d.out = make([]byte, len(decodeBuf)-remain)
				copy(d.out, decodeBuf[remain:])
				d.nbuf = target
				return d.nbuf, nil
			}
		}
		if err != nil {
			return d.nbuf, err
		}
	}
}

// DecodedLen figure out how long the decoded result will be
func DecodedLen(n int) int {
	return ((n / fullEncodedBlockSize) * fullBlockSize) + blockSizes[n%fullEncodedBlockSize]
}

// Decode base58 decode the src and write result to dst
func Decode(dst, src []byte) (int, error) {
	var n int
	for len(src) >= fullEncodedBlockSize {
		nn, err := decodeBlock(dst, src[:fullEncodedBlockSize])
		n += nn
		if err != nil {
			return nn, err
		}
		dst = dst[nn:]
		src = src[fullEncodedBlockSize:]
	}

	if len(src) != 0 {
		nn, err := decodeBlock(dst, src)
		n += nn
		if err != nil {
			return nn, err
		}
	}
	return n, nil
}

// DecodeString base58 decode the given string , and return raw bytes
func DecodeString(s string) ([]byte, error) {
	b := make([]byte, DecodedLen(len(s)))
	_, err := Decode(b, []byte(s))
	return b, err
}
