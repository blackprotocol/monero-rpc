package base58

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"

	moneroCrypto "gitlab.com/blackprotocol/monero-rpc/crypto"
)

func encodeBlock(dst, src []byte) {
	var num uint64
	var i int
	if len(src) > fullBlockSize {
		num = uint8beTo64(src[:fullBlockSize])
		i = fullEncodedBlockSize
	} else {
		num = uint8beTo64(src)
		i = encodedBlockSizes[len(src)]
	}
	i--
	for i >= 0 {
		remainder := num % alphabetSize
		num /= alphabetSize
		dst[i] = alphabet[remainder]
		i--
	}
}

func uint8beTo64(b []byte) (r uint64) {
	i := 0
	switch 9 - len(b) {
	case 1:
		r |= uint64(b[i])
		i++
		fallthrough
	case 2:
		r <<= 8
		r |= uint64(b[i])
		i++
		fallthrough
	case 3:
		r <<= 8
		r |= uint64(b[i])
		i++
		fallthrough
	case 4:
		r <<= 8
		r |= uint64(b[i])
		i++
		fallthrough
	case 5:
		r <<= 8
		r |= uint64(b[i])
		i++
		fallthrough
	case 6:
		r <<= 8
		r |= uint64(b[i])
		i++
		fallthrough
	case 7:
		r <<= 8
		r |= uint64(b[i])
		i++
		fallthrough
	case 8:
		r <<= 8
		r |= uint64(b[i])
	}
	return r
}

// EncodeAddress follow monero address format to encoded the given tag and data into a monero address
func EncodeAddress(tag uint64, data []byte) (string, error) {
	var buf bytes.Buffer
	enc := NewEncoder(&buf)
	hash := moneroCrypto.NewHash()
	mw := io.MultiWriter(enc, hash)
	b := make([]byte, 16)
	n := binary.PutUvarint(b, tag)
	if _, err := mw.Write(b[:n]); err != nil {
		return "", fmt.Errorf("fail to write tag,err: %w", err)
	}
	if _, err := mw.Write(data); err != nil {
		return "", fmt.Errorf("fail to write data,err: %w", err)
	}

	digest := hash.Sum(nil)
	if _, err := enc.Write(digest[:checksumSize]); err != nil {
		return "", fmt.Errorf("fail to write checksum,err: %w", err)
	}
	if err := enc.Close(); err != nil {
		return "", fmt.Errorf("fail to close encoder,err: %w", err)
	}
	return buf.String(), nil
}

type encoder struct {
	err  error
	w    io.Writer
	buf  [fullBlockSize]byte
	nbuf int
	out  [fullEncodedBlockSize]byte
}

// NewEncoder create a new instance of bse58 encoder
func NewEncoder(w io.Writer) io.WriteCloser {
	return &encoder{w: w}
}

// Write encode the given bytes
// returns n to indicate how many bytes have been written successfully
// err to indicate error
func (e *encoder) Write(p []byte) (n int, err error) {
	if e.err != nil {
		return 0, e.err
	}

	// Leading fringe.
	if e.nbuf > 0 {
		var i int
		for i = 0; i < len(p) && e.nbuf < fullBlockSize; i++ {
			e.buf[e.nbuf] = p[i]
			e.nbuf++
		}
		n += i
		p = p[i:]
		if e.nbuf < fullBlockSize {
			return
		}
		encodeBlock(e.out[0:], e.buf[0:])
		if _, e.err = e.w.Write(e.out[0:]); e.err != nil {
			return n, e.err
		}
		e.nbuf = 0
	}

	// Full interior blocks.
	for len(p) >= fullBlockSize {
		encodeBlock(e.out[0:], p[0:fullBlockSize])
		if _, e.err = e.w.Write(e.out[0:]); err != nil {
			return n, err
		}
		n += fullBlockSize
		p = p[fullBlockSize:]
	}

	// Trailing fringe.
	for i := 0; i < len(p); i++ {
		e.buf[i] = p[i]
	}
	e.nbuf = len(p)
	n += len(p)
	return
}

// Close flush all the bytes, encode and write it all to the output
func (e *encoder) Close() error {
	if e.err == nil && e.nbuf > 0 {
		encodeBlock(e.out[0:], e.buf[0:e.nbuf])
		_, e.err = e.w.Write(e.out[0:encodedBlockSizes[e.nbuf]])
		e.nbuf = 0
	}
	return e.err
}

// EncodedLen calculate how long the encoded result will be
func EncodedLen(n int) int {
	return ((n / fullBlockSize) * fullEncodedBlockSize) + encodedBlockSizes[n%fullBlockSize]
}

// Encode the given src bytes slice , and put the result into dst
func Encode(dst, src []byte) {
	if len(src) == 0 {
		return
	}
	fullBlockCount := len(src) / fullBlockSize
	lastBlockSize := len(src) % fullBlockSize

	for i := 0; i < fullBlockCount; i++ {
		encodeBlock(dst[i*fullEncodedBlockSize:], src[i*fullBlockSize:])
	}
	if lastBlockSize > 0 {
		encodeBlock(dst[fullBlockCount*fullEncodedBlockSize:], src[fullBlockCount*fullBlockSize:])
	}
}

// EncodeToString encode the given input byte slice to string
func EncodeToString(src []byte) (string, error) {
	var buf bytes.Buffer
	enc := NewEncoder(&buf)
	_, err := enc.Write(src)
	if err != nil {
		return "", fmt.Errorf("fail to write ,err: %w", err)
	}
	if err := enc.Close(); err != nil {
		return "", fmt.Errorf("fail to close encoder")
	}
	return buf.String(), nil
}
