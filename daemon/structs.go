package daemon

// ResponseGetBlockCount response from get_block_count
type ResponseGetBlockCount struct {
	Count     uint64 `json:"count"`     // Count Number of blocks in longest chain seen by the node.
	Status    string `json:"status"`    // Status general RPC error code. "OK" means everything looks good
	UnTrusted bool   `json:"untrusted"` // UnTrusted States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestGetBlockTemplate get a block template on which mining a new block. (get_block_template)
type RequestGetBlockTemplate struct {
	WalletAddress string `json:"wallet_address"` // WalletAddress Address of wallet to receive coinbase transactions if block is successfully mined.
	ReserveSize   uint64 `json:"reserve_size"`   // Reserve size
}

// ResponseGetBlockTemplate response of get_block_template
type ResponseGetBlockTemplate struct {
	BlockTemplateBlob string `json:"blocktemplate_blob"` // Blob on which to try to mine a new block
	BlockHashingBlob  string `json:"blockhashing_blob"`  // Blob on which to try to find a valid nonce
	Difficulty        uint64 `json:"difficulty"`         // Least-significant 64 bits of the 128-bit network difficulty
	DifficultyTop64   uint64 `json:"difficulty_top64"`   // Most-significant 64 bits of the 128-bit network difficulty
	ExpectedReward    uint64 `json:"expected_reward"`    // Coinbase reward expected to be received if block is successfully mined
	Height            uint64 `json:"height"`             // Height on which to mine.
	NextSeedHash      string `json:"next_seed_hash"`     // Hash of the next block to use as seed for Random-X proof-of-work.
	PrevHash          string `json:"prev_hash"`          // Hash of the most recent block on which to mine the next block
	ReservedOffset    uint64 `json:"reserved_offset"`    // Reserved offset
	SeedHash          string `json:"seed_hash"`          // Hash of block to use as seed for Random-X proof-of-work
	SeedHeight        uint64 `json:"seed_height"`        // Height of block to use as seed for Random-X proof-of-work
	Status            string `json:"status"`             //  General RPC error code. "OK" means everything looks good.	untrusted - boolean; States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	WideDifficulty    string `json:"wide_difficulty"`    // Network difficulty (analogous to the strength of the network) as a hexadecimal string representing a 128-bit number.
}

// RequestSubmitBlock Submit a mined block to the network
type RequestSubmitBlock struct {
	BlockBlobDatas []string `json:"block_blob_datas"` // list of block blobs which have been mined
}

// ResponseSubmitBlock Response
type ResponseSubmitBlock struct {
	Status string `json:"status"` // Block submit status.
}

// BlockHeader struct containing block header information
type BlockHeader struct {
	BlockSize                 uint64 `json:"block_size"`                  // Backward compatibility, same as block_weight, use that instead
	BlockWeight               uint64 `json:"block_weight"`                // The adjusted block size, in bytes. This is the raw size, plus a positive adjustment for any Bulletproof transactions with more than 2 outputs.
	CumulativeDifficulty      uint64 `json:"cumulative_difficulty"`       // Least-significant 64 bits of the cumulative difficulty of all blocks up to the block in the reply.
	CumulativeDifficultyTop64 uint64 `json:"cumulative_difficulty_top64"` // Most-significant 64 bits of the 128-bit cumulative difficulty.
	Depth                     uint64 `json:"depth"`                       // The number of blocks succeeding this block on the blockchain. A larger number means an older block
	Difficulty                uint64 `json:"difficulty"`                  // The strength of the Monero network based on mining power.
	Hash                      string `json:"hash"`                        // The hash of this block
	Height                    uint64 `json:"height"`                      // The number of blocks preceding this block on the blockchain
	LongTermWeight            uint64 `json:"long_term_weight"`            // The long term block weight, based on the median weight of the preceding 100000 blocks
	MajorVersion              uint64 `json:"major_version"`               // The major version of the monero protocol at this block height
	MinerTxHash               string `json:"miner_tx_hash"`               // The hash of this block's coinbase transaction
	MinorVersion              uint64 `json:"minor_version"`               // The minor version of the monero protocol at this block height
	Nonce                     uint64 `json:"nonce"`                       // a cryptographic random one-time number used in mining a Monero block
	NumTxs                    uint64 `json:"num_txs"`                     // Number of transactions in the block, not counting the coinbase tx
	OrphanStatus              bool   `json:"orphan_status"`               // Usually false. If true, this block is not part of the longest chain
	POWHash                   string `json:"pow_hash"`                    // The hash, as a hexadecimal string, calculated from the block as proof-of-work
	PrevHash                  string `json:"prev_hash"`                   // The hash of the block immediately preceding this block in the chain.
	Reward                    uint64 `json:"reward"`                      // The amount of new atomic units generated in this block and rewarded to the miner. Note: 1 XMR = 1e12 atomic units
	Timestamp                 uint64 `json:"timestamp"`                   // The unix time at which the block was recorded into the blockchain
	WideCumulativeDifficulty  string `json:"wide_cumulative_difficulty"`  // Cumulative difficulty of all blocks in the blockchain as a hexadecimal string representing a 128-bit number
	WideDifficulty            string `json:"wide_difficulty"`             // Network difficulty (analogous to the strength of the network) as a hexadecimal string representing a 128-bit number
}

// ResponseGetLastBlockHeader response of get_last_block_header
type ResponseGetLastBlockHeader struct {
	BlockHeader BlockHeader `json:"block_header"`
	Credit      uint64      `json:"credit"`    // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	Status      string      `json:"status"`    // General RPC error code. "OK" means everything looks good
	TopHash     string      `json:"top_hash"`  // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty
	UnTrusted   bool        `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestGetBlockHeaderByHash request get block header by hash
type RequestGetBlockHeaderByHash struct {
	Hash string `json:"hash"`
}

// ResponseGetBlockHeaderByHash response of get_block_header_by_hash
type ResponseGetBlockHeaderByHash struct {
	BlockHeader BlockHeader `json:"block_header"`
	Status      string      `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted   bool        `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestGetBlockHeaderByHeight get block header by height request
type RequestGetBlockHeaderByHeight struct {
	Height uint64 `json:"height"` // block height
}

// ResponseGetBlockHeaderByHeight get block header by height response
type ResponseGetBlockHeaderByHeight struct {
	BlockHeader BlockHeader `json:"block_header"`
	Status      string      `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted   bool        `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestGetBlockHeadersRange get block headers by range
type RequestGetBlockHeadersRange struct {
	StartHeight uint64 `json:"start_height"` // The starting block's height.
	EndHeight   uint64 `json:"end_height"`   // The ending block's height
}

// ResponseGetBlockHeadersRange response of get block headers by range
type ResponseGetBlockHeadersRange struct {
	Credits   uint64        `json:"credits"`   // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	Headers   []BlockHeader `json:"headers"`   // Block headers
	Status    string        `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool          `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	TopHash   string        `json:"top_hash"`  // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty
}

// RequestGetBlock request to get block
type RequestGetBlock struct {
	Height uint64 `json:"height"` // block height
	Hash   string `json:"hash"`   // block hash
}

// ResponseGetBlock response of get_block
type ResponseGetBlock struct {
	Blob        string      `json:"blob"`         // Hexadecimal blob of block information
	BlockHeader BlockHeader `json:"block_header"` // block header
	Credits     uint64      `json:"credits"`      // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	Json        string      `json:"json"`         // JSON formatted block details
	Status      string      `json:"status"`       // General RPC error code. "OK" means everything looks good.
	UnTrusted   bool        `json:"untrusted"`    // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	TopHash     string      `json:"top_hash"`     // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	BlockDetail BlockDetail // BlockDetail information , parsed from `json` field
}

// BlockDetail information parsed from json field of ResponseGetBlock
type BlockDetail struct {
	MajorVersion uint64 `json:"major_version"`
	MinorVersion uint64 `json:"minor_version"`
	Timestamp    uint64 `json:"timestamp"`
	PrevID       string `json:"prev_id"` // prev hash
	Nonce        uint64 `json:"nonce"`
	MinerTx      struct {
		Version    uint64 `json:"version"`
		UnlockTime uint64 `json:"unlock_time"` // The block height when the coinbase transaction becomes spendable.
		Vin        []struct {
			Gen struct {
				Height uint64 `json:"height"` // This block height, a.k.a. when the coinbase is generated
			} `json:"gen"`
		} `json:"vin"`
		VOut []struct {
			Amount uint64 `json:"amount"` // The amount of the output, in atomic units.
			Target struct {
				TaggedKey struct {
					Key     string `json:"key"`
					ViewTag string `json:"view_tag"`
				} `json:"tagged_key"`
			} `json:"target"`
		} `json:"vout"`
		Extra      []byte   `json:"extra"`     // Usually called the "transaction ID" but can be used to include any random 32 byte/64 character hex string
		Signatures []string `json:"signature"` // Contain signatures of tx signers. Coinbased txs do not have signatures
	} `json:"miner_tx"` // List of hashes of non-coinbase transactions in the block. If there are no other transactions, this will be an empty list
	TxHashes []string `json:"tx_hashes"`
}

// ConnectionDetail connection information
type ConnectionDetail struct {
	Address         string `json:"address"`          // The peer's address, actually IPv4 & port
	AvgDownload     uint64 `json:"avg_download"`     // Average bytes of data downloaded by node
	AvgUpload       uint64 `json:"avg_upload"`       // Average bytes of data uploaded by node
	ConnectionID    string `json:"connection_id"`    // The connection ID
	CurrentDownload uint64 `json:"current_download"` // Current bytes downloaded by node
	CurrentUpload   uint64 `json:"current_upload"`   // Current bytes uploaded by node.
	Height          uint64 `json:"height"`           // The peer height
	Host            string `json:"host"`             // The peer host
	Incoming        bool   `json:"incoming"`         // Is the node getting information from your node?
	IP              string `json:"ip"`               // The node's IP address.
	LiveTime        uint64 `json:"live_time"`
	LocalIP         bool   `json:"local_ip"`
	Localhost       bool   `json:"localhost"`
	PeerID          string `json:"peer_id"` // The node's ID on the network
	Port            string `json:"port"`    // The port that the node is using to connect to the network
	RecvCount       uint64 `json:"recv_count"`
	RecvIdleTime    uint64 `json:"recv_idle_time"`
	SendCount       uint64 `json:"send_count"`
	SendIdleTime    uint64 `json:"send_idle_time"`
	State           string `json:"state"`
	SupportFlags    uint64 `json:"support_flags"`
}

// ResponseGetConnections Retrieve information about incoming and outgoing connections to your node
type ResponseGetConnections struct {
	Connections []ConnectionDetail `json:"connections"`
	Status      string             `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted   bool               `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// ResponseGetInfo response of get_info RPC call
type ResponseGetInfo struct {
	AdjustedTime              uint64 `json:"adjusted_time"`               // Current time approximated from chain data, as Unix time.
	AltBlocksCount            uint64 `json:"alt_blocks_count"`            // Number of alternative blocks to main chain.
	BlockSizeLimit            uint64 `json:"block_size_limit"`            // Backward compatibility, same as block_weight_limit, use that instead
	BlockSizeMedian           uint64 `json:"block_size_median"`           // Backward compatibility, same as block_weight_median, use that instead
	BlockWeightLimit          uint64 `json:"block_weight_limit"`          // Maximum allowed adjusted block size based on latest 100000 blocks
	BlockWeightMedian         uint64 `json:"block_weight_median"`         // Median adjusted block size of latest 100000 blocks
	BootstrapDaemonAddress    string `json:"bootstrap_daemon_address"`    // Bootstrap node to give immediate usability to wallets while syncing by proxying RPC to it. (Note: the replies may be untrustworthy).
	BusySyncing               bool   `json:"busy_syncing"`                // States if new blocks are being added (true) or not (false).
	Credits                   uint64 `json:"credits"`                     // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	CumulativeDifficulty      uint64 `json:"cumulative_difficulty"`       // Least-significant 64 bits of the 128-bit cumulative difficulty.
	CumulativeDifficultyTop64 uint64 `json:"cumulative_difficulty_top64"` // Most-significant 64 bits of the 128-bit cumulative difficulty.
	DatabaseSize              uint64 `json:"database_size"`               // The size of the blockchain database, in bytes.
	Difficulty                uint64 `json:"difficulty"`                  // Least-significant 64 bits of the 128-bit network difficulty.
	DifficultyTop64           uint64 `json:"difficulty_top64"`            // Most-significant 64 bits of the 128-bit network difficulty.
	FreeSpace                 uint64 `json:"free_space"`                  // Available disk space on the node.
	GreyPeerListSize          uint64 `json:"grey_peerlist_size"`          // Grey Peerlist Size
	Height                    uint64 `json:"height"`                      // Current length of longest chain known to daemon.
	HeightWithoutBootstrap    uint64 `json:"height_without_bootstrap"`    // Current length of the local chain of the daemon.
	IncomingConnectionsCount  uint64 `json:"incoming_connections_count"`  // Number of peers connected to and pulling from your node.
	Mainnet                   bool   `json:"mainnet"`                     // States if the node is on the mainnet (true) or not (false).
	NetType                   string `json:"nettype"`                     // Network type (one of mainnet, stagenet or testnet).
	Offline                   bool   `json:"offline"`                     // States if the node is offline (true) or online (false).
	OutgoingConnectionsCount  uint64 `json:"outgoing_connections_count"`  // Number of peers that you are connected to and getting information from.
	RPCConnectionsCount       uint64 `json:"rpc_connections_count"`       // Number of RPC client connected to the daemon (Including this RPC request).
	StageNet                  bool   `json:"stagenet"`                    // States if the node is on the stagenet (true) or not (false).
	StartTime                 uint64 `json:"start_time"`                  // Start time of the daemon, as UNIX time.
	Status                    string `json:"status"`                      // General RPC error code. "OK" means everything looks good.
	Synchronized              bool   `json:"synchronized"`                // States if the node is synchronized (true) or not (false).
	Target                    uint64 `json:"target"`                      // Current target for next proof of work.
	TargetHeight              uint64 `json:"target_height"`               // The height of the next block in the chain.
	Testnet                   bool   `json:"testnet"`                     // States if the node is on the testnet (true) or not (false).
	TopBlockHash              string `json:"top_block_hash"`              // Hash of the highest block in the chain.
	TopHash                   string `json:"top_hash"`                    // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	TxCount                   uint64 `json:"tx_count"`                    // Total number of non-coinbase transaction in the chain.
	TxPoolSize                uint64 `json:"tx_pool_size"`                // Number of transactions that have been broadcast but not included in a block.
	Untrusted                 bool   `json:"untrusted"`                   // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	UpdateAvailable           bool   `json:"update_available"`            // States if a newer Monero software version is available.
	Version                   string `json:"version"`                     // The version of the Monero software the node is running.
	WasBootstrapEverUsed      bool   `json:"was_bootstrap_ever_used"`     // States if a bootstrap node has ever been used since the daemon started.
	WhitePeerListSize         uint64 `json:"white_peerlist_size"`         // White Peerlist Size
	WideCumulativeDifficulty  string `json:"wide_cumulative_difficulty"`  // Cumulative difficulty of all blocks in the blockchain as a hexadecimal string representing a 128-bit number
	WideDifficulty            string `json:"wide_difficulty"`             // Network difficulty (analogous to the strength of the network) as a hexadecimal string representing a 128-bit number
}

// ResponseHardForkInfo response of (hard_fork_info)
type ResponseHardForkInfo struct {
	Credits        uint64 `json:"credits"`         // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	EarliestHeight uint64 `json:"earliest_height"` // Block height at which hard fork would be enabled if voted in.
	Enabled        bool   `json:"enabled"`         // Tells if hard fork is enforced.
	State          uint64 `json:"state"`           // Current hard fork state: 0 (There is likely a hard fork), 1 (An update is needed to fork properly), or 2 (Everything looks good).
	Status         string `json:"status"`          // General RPC error code. "OK" means everything looks good.
	Threshold      uint64 `json:"threshold"`       // Minimum percent of votes to trigger hard fork. Default is 80.
	TopHash        string `json:"top_hash"`        // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	Untrusted      bool   `json:"untrusted"`       // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	Version        uint64 `json:"version"`         // The major block version for the fork.
	Votes          uint64 `json:"votes"`           // Number of votes towards hard fork.
	Voting         uint64 `json:"voting"`          // Hard fork voting status.
	Window         uint64 `json:"window"`          // Number of blocks over which current votes are cast. Default is 10080 blocks.
}

// RequestSetBan Ban another node by IP request to set_ban
type RequestSetBan struct {
	Bans []struct {
		Host    string `json:"host"`    // Host to ban (IP in A.B.C.D form - will support I2P address in the future).
		IP      uint64 `json:"ip"`      // IP address to ban, in Int format
		Ban     bool   `json:"ban"`     // Set true to ban.
		Seconds uint64 `json:"seconds"` // Number of seconds to ban node
	} `json:"bans"`
}

// ResponseSetBan response of set_ban
type ResponseSetBan struct {
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// ResponseGetBans response of get_bans
type ResponseGetBans struct {
	Bans []struct {
		Host    string `json:"host"`    // Host to ban (IP in A.B.C.D form - will support I2P address in the future).
		IP      uint64 `json:"ip"`      // IP address to ban, in Int format
		Seconds uint64 `json:"seconds"` // Number of seconds to ban node
	} `json:"bans"`
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestFlushTxPool Flush tx ids from transaction pool
type RequestFlushTxPool struct {
	TxIDs []string `json:"txids"` // Optional, list of transactions IDs to flush from pool (all tx ids flushed if empty).
}

// ResponseFlushTxPool response of flush_txpool
type ResponseFlushTxPool struct {
	Status string `json:"status"` // General RPC error code. "OK" means everything looks good.
}

// RequestGetOutputHistogram Get a histogram of output amounts. For all amounts (possibly filtered by parameters), gives the number of outputs on the chain for that amount. RingCT outputs counts as 0 amount
type RequestGetOutputHistogram struct {
	Amounts      []uint64 `json:"amounts"`
	MinCount     uint64   `json:"min_count"`
	MaxCount     uint64   `json:"max_count"`
	Unlocked     bool     `json:"unlocked"`
	RecentCutoff uint64   `json:"recent_cutoff"`
}

// ResponseGetOutputHistogram response of get_output_histogram
type ResponseGetOutputHistogram struct {
	Credits   uint64 `json:"credits"` // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	Histogram []struct {
		Amount            uint64 `json:"amount"` // Output amount in atomic units
		TotalInstances    uint64 `json:"total_instances"`
		UnLockedInstances uint64 `json:"unlocked_instances"`
		RecentInstances   uint64 `json:"recent_instances"`
	} `json:"histogram"` // list of histogram entries
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	TopHash   string `json:"top_hash"`  // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestGetCoinbaseTxSum Get the coinbase amount and the fees amount for n last blocks starting at particular height
type RequestGetCoinbaseTxSum struct {
	Height uint64 `json:"height"` // Block height from which getting the amounts
	Count  uint64 `json:"count"`  // number of blocks to include in the sum
}

// ResponseGetCoinbaseTxSum response from get_coinbase_tx_sum
type ResponseGetCoinbaseTxSum struct {
	Credits             uint64 `json:"credits"`               // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	EmissionAmount      uint64 `json:"emission_amount"`       // Least significant 64 bits for 128 bit integer representing the sum of coinbase rewards in atomic units
	EmissionAmountTop64 uint64 `json:"emission_amount_top64"` // Most significant 64 bits for 128 bit integer representing the sum of coinbase rewards in atomic units
	FeeAmount           uint64 `json:"fee_amount"`            // Most significant 64 bits for 128 bit integer representing the sum of fees in atomic units
	FeeAmountTop64      uint64 `json:"fee_amount_top64"`      // Most significant 64 bits for 128 bit integer representing the sum of fees in atomic units
	Status              string `json:"status"`                // General RPC error code. "OK" means everything looks good.
	TopHash             string `json:"top_hash"`              // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	UnTrusted           bool   `json:"untrusted"`             // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	WideEmissionAmount  string `json:"wide_emission_amount"`  // 128 bit hex encoded integer; Sum of coinbase rewards in atomic units.
	WideFeeAmount       string `json:"wide_fee_amount"`       // 128 bit hex encoded integer; Sum of fees in atomic units.
}

// ResponseGetVersion response from get_version
type ResponseGetVersion struct {
	Release   bool   `json:"release"`   // States if the daemon software version corresponds to an official tagged release (true), or not (false)
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	Version   uint64 `json:"version"`
}

// RequestGetFeeEstimate Gives an estimation on fees per byte.
type RequestGetFeeEstimate struct {
	GraceBlocks uint64 `json:"grace_blocks"` // optional
}

// ResponseGetFeeEstimate response from get_fee_estimate
type ResponseGetFeeEstimate struct {
	Credits          uint64 `json:"credits"`           // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	Fee              uint64 `json:"fee"`               // Amount of fees estimated per byte in atomic units
	QuantizationMask uint64 `json:"quantization_mask"` // Final fee should be rounded up to an even multiple of this value
	Status           string `json:"status"`            // General RPC error code. "OK" means everything looks good.
	TopHash          string `json:"top_hash"`          // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	UnTrusted        bool   `json:"untrusted"`         // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// ResponseGetAlternateChains response from get_alternate_chains
type ResponseGetAlternateChains struct {
	Chains []struct {
		BlockHash            string   `json:"block_hash"`              // the block hash of the first diverging block of this alternative chain.
		BlockHashes          []string `json:"block_hashes"`            // An array of all block hashes in the alternative chain that are not in the main chain.
		Difficulty           uint64   `json:"difficulty"`              // Least-significant 64 bits of 128-bit integer for the cumulative difficulty of all blocks in the alternative chain.
		DifficultyTop64      uint64   `json:"difficulty_top64"`        // Most-significant 64 bits of the 128-bit network difficulty.
		Height               uint64   `json:"height"`                  // the block height of the first diverging block of this alternative chain.
		Length               uint64   `json:"length"`                  // the length in blocks of this alternative chain, after divergence.
		MainChainParentBlock string   `json:"main_chain_parent_block"` // The hash of the greatest height block that is shared between the alternative chain and the main chain.
		WideDifficulty       string   `json:"wide_difficulty"`         // Network difficulty (analogous to the strength of the network) as a hexadecimal string representing a 128-bit number.
	} `json:"chains"` // array of chains
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestRelayTx Relay a list of transaction IDs
type RequestRelayTx struct {
	TxIDs []string `json:"txids"` // list of transaction IDs to relay
}

// ResponseRelayTx response from relay_tx
type ResponseRelayTx struct {
	Status string `json:"status"` // General RPC error code. "OK" means everything looks good.
}

// ResponseSyncInfo response from sync_info
type ResponseSyncInfo struct {
	Credits               uint64 `json:"credits"`                  // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	Height                uint64 `json:"height"`                   // height
	NextNeededPruningSeed uint64 `json:"next_needed_pruning_seed"` // The next pruning seed needed for pruned sync.
	Overview              string `json:"overview"`                 // Overview of current block queue where each character in the string represents a block set in the queue. . = requested but not received, o = set received, m  = received set that matches the next blocks needed
	Peers                 []struct {
		Info ConnectionDetail `json:"info"`
	} `json:"peers"` // array of peer structure
	Spans []struct {
		ConnectionID     string `json:"connection_id"`      // id of connection
		NBlocks          uint64 `json:"n_blocks"`           // number of blocks in that span
		Rate             uint64 `json:"rate"`               // connection rate
		RemoteAddress    string `json:"remote_address"`     // peer address the node is downloading (or has downloaded) than span from
		Size             uint64 `json:"size"`               // total number of bytes in that span's blocks (including txes)
		Speed            uint64 `json:"speed"`              // connection speed
		StartBlockHeight uint64 `json:"start_block_height"` // block height of the first block in that span
	} `json:"spans"` // array of span structure, (optional, absent if node is fully synced)
	Status       string `json:"status"`        // General RPC error code. "OK" means everything looks good.
	TargetHeight uint64 `json:"target_height"` // target height the node is syncing from (will be 0 if node is fully synced)
}

// ResponseGetTxPoolBacklog Get all transaction pool backlog
type ResponseGetTxPoolBacklog struct {
	Backlog []struct {
		BlobSize   uint64 `json:"blob_size"`    // blob size in binary form
		Fee        uint64 `json:"fee"`          // fee in binary form
		TimeInPool uint64 `json:"time_in_pool"` // Time in pool , in binary form
	}
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestGetOutputDistribution (get_output_distribution)
type RequestGetOutputDistribution struct {
	Amounts    []uint64 `json:"amounts"`     // amounts to look for
	Cumulative bool     `json:"cumulative"`  // (optional, default is false) States if the result should be cumulative (true) or not (false)
	FromHeight uint64   `json:"from_height"` // (optional, default is 0) starting height to check from
	ToHeight   uint64   `json:"to_height"`   // (optional, default is 0) ending height to check up to
}

// ResponseGetOutputDistribution response from get_output_distribution
type ResponseGetOutputDistribution struct {
	Distributions []struct {
		Amount       uint64   `json:"amount"`
		Base         uint64   `json:"base"`
		Distribution []uint64 `json:"distribution"`
		StartHeight  uint64   `json:"start_height"`
	} `json:"distributions"`
	Status string `json:"status"` // General RPC error code. "OK" means everything looks good.
}

// RequestGetTransactions Look up one or more transactions by hash
type RequestGetTransactions struct {
	TxsHashes    []string `json:"txs_hashes"`     // List of transaction hashes to look up
	DecodeAsJson bool     `json:"decode_as_json"` // Optional (false by default). If set true, the returned transaction information will be decoded rather than binary.
	Prune        bool     `json:"prune"`          // Optional (false by default).
}

// ResponseGetTransactions response from get_transactions
type ResponseGetTransactions struct {
	MissedTx []string `json:"missed_tx"` // (Optional - returned if not empty) Transaction hashes that could not be found.
	Status   string   `json:"status"`    // General RPC error code. "OK" means everything looks good.
	TopHash  string   `json:"top_hash"`  // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	Txs      []struct {
		AsHex           string `json:"as_hex"`
		AsJson          string `json:"as_json"`
		Detail          TransactionDetail
		BlockHeight     uint64   `json:"block_height"`
		BlockTimestamp  uint64   `json:"block_timestamp"`
		DoubleSpendSeen bool     `json:"double_spend_seen"`
		InPool          bool     `json:"in_pool"`
		OutputIndices   []uint64 `json:"output_indices"`
		PrunableAsHex   string   `json:"prunable_as_hex"`
		PrunableHex     string   `json:"prunable_hex"`
		TxHash          string   `json:"tx_hash"` // transaction hash
	} `json:"txs"`
	TxsAsHex  []string `json:"txs_as_hex"`  // Full transaction information as a hex string (old compatibility parameter)
	TxsAsJson []string `json:"txs_as_json"` // (Optional - returned if set in inputs. Old compatibility parameter) List of transaction as in as_json above
}
type TransactionDetail struct {
	Version    uint64 `json:"version"`
	UnlockTime uint64 `json:"unlock_time"` // The block height when the coinbase transaction becomes spendable.
	Vin        []struct {
		Key struct {
			Amount     uint64 `json:"amount"`
			KeyOffsets []byte `json:"key_offsets"`
			KeyImage   string `json:"k_image"`
		} `json:"key"`
	} `json:"vin"`
	VOut []struct {
		Amount uint64 `json:"amount"` // The amount of the output, in atomic units.
		Target struct {
			TaggedKey struct {
				Key     string `json:"key"`
				ViewTag string `json:"view_tag"`
			} `json:"tagged_key"`
		} `json:"target"`
	} `json:"vout"`
	Extra         []byte `json:"extra"` // Usually called the "transaction ID" but can be used to include any random 32 byte/64 character hex string
	RCTSignatures struct {
		Type     uint64 `json:"type"`
		TxnFee   uint64 `json:"txnFee"`
		ECDHInfo []struct {
			Amount string `json:"amount"`
		} `json:"ecdhInfo"`
		OutPK []string `json:"out_pk"`
	} `json:"rct_signatures"`
	RCTSigPrunable struct {
		NBP uint64 `json:"nbp"`
		BPP []struct {
			A  string   `json:"A"`
			A1 string   `json:"A1"`
			B  string   `json:"B"`
			R1 string   `json:"r1"`
			S1 string   `json:"s1"`
			D1 string   `json:"d1"`
			L  []string `json:"L"`
			R  []string `json:"R"`
		} `json:"bpp"`
		CLSAGs []struct {
			S  []string `json:"s"`
			C1 string   `json:"c1"`
			D  string   `json:"D"`
		} `json:"CLSAGs"`
	} `json:"rctsig_prunable"`
	PseudoOuts []string `json:"pseudoOuts"`
}

// RequestIsKeyImageSpent Check if outputs have been spent using the key image associated with the output
type RequestIsKeyImageSpent struct {
	KeyImages []string `json:"key_images"`
}
type ResponseIsKeyImageSpent struct {
	Credits     uint64   `json:"credits"`      // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	SpentStatus []uint64 `json:"spent_status"` // List of statuses for each image checked. Statuses are follows: 0 = unspent, 1 = spent in blockchain, 2 = spent in transaction pool
	TopHash     string   `json:"top_hash"`     // If payment for RPC is enabled, the hash of the highest block in the chain. Otherwise, empty.
	Status      string   `json:"status"`       // General RPC error code. "OK" means everything looks good.
	UnTrusted   bool     `json:"untrusted"`    // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestSendRawTransaction  Broadcast a raw transaction to the network
type RequestSendRawTransaction struct {
	TxAsHex    string `json:"tx_as_hex"`    // Full transaction information as hexidecimal string.
	DoNotRelay bool   `json:"do_not_relay"` // Stop relaying transaction to other nodes (default is false).
}

// ResponseSendRawTransaction response from send_raw_transaction
type ResponseSendRawTransaction struct {
	DoubleSpend   bool   `json:"double_spend"`   // Transaction is a double spend (true) or not (false).
	FeeTooLow     bool   `json:"fee_too_low"`    // Fee is too low (true) or OK (false).
	InvalidInput  bool   `json:"invalid_input"`  // Input is invalid (true) or valid (false).
	InvalidOutput bool   `json:"invalid_output"` // Output is invalid (true) or valid (false).
	LowMixin      bool   `json:"low_mixin"`      // Mixin count is too low (true) or OK (false).
	NotRct        bool   `json:"not_rct"`        // Transaction is a standard ring transaction (true) or a ring confidential transaction (false).
	NotRelayed    bool   `json:"not_relayed"`    //  Transaction was not relayed (true) or relayed (false).
	Overspend     bool   `json:"overspend"`      // Transaction uses more money than available (true) or not (false).
	Reason        string `json:"reason"`         // Additional information. Currently empty or "Not relayed" if transaction was accepted but not relayed.
	Status        string `json:"status"`         // General RPC error code. "OK" means everything looks good.
	TooBig        bool   `json:"too_big"`        // Transaction size is too big (true) or OK (false).
	UnTrusted     bool   `json:"untrusted"`      // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// Peer node
type Peer struct {
	Host     uint64 `json:"host"`      // IP address in integer format
	ID       string `json:"id"`        // Peer id
	IP       uint64 `json:"ip"`        // IP address in integer format
	LastSeen uint64 `json:"last_seen"` // unix time at which the peer has been seen for the last time
	Port     uint64 `json:"port"`      // TCP port the peer is using to connect to monero network.
}

// ResponseGetPeerList get the known peers list
type ResponseGetPeerList struct {
	GrayList  []Peer `json:"gray_list"`  // array of offline peer
	Status    string `json:"status"`     // General RPC error code. "OK" means everything looks good.
	WhiteList []Peer `json:"white_list"` // array of online peer
}

// ResponseGetTransactionPool Show information about valid transactions seen by the node but not yet mined into a block, as well as spent key image information for the txpool in the node's memory. response of get_transaction_pool
type ResponseGetTransactionPool struct {
	Credit         uint64 `json:"credit"` // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	SpentKeyImages []struct {
		IDHash    string   `json:"id_hash"`    // Key image
		TxsHashes []string `json:"txs_hashes"` // x hashes of the txes (usually one) spending that key image.
	} `json:"spent_key_images"` // List of spent output key images
	Status       string `json:"status"` // General RPC error code. "OK" means everything looks good.
	Transactions []struct {
		BlobSize           uint64 `json:"blob_size"`             // The size of the full transaction blob.
		DoNotRelay         bool   `json:"do_not_relay"`          // States if this transaction should not be relayed
		DoubleSpendSeen    bool   `json:"double_spend_seen"`     // States if this transaction has been seen as double spend.
		Fee                uint64 `json:"fee"`                   // The amount of the mining fee included in the transaction, in atomic units
		IDHash             string `json:"id_hash"`               // The transaction ID hash
		KeptByBlock        bool   `json:"kept_by_block"`         // States if the tx was included in a block at least once (true) or not (false).
		LastFailedHeight   uint64 `json:"last_failed_height"`    // If the transaction validation has previously failed, this tells at what height that occured
		LastFailedIDHash   string `json:"last_failed_id_hash"`   // this tells the previous transaction ID hash.
		LastRelayedTime    uint64 `json:"last_relayed_time"`     // Last unix time at which the transaction has been relayed
		MaxUsedBlockHeight uint64 `json:"max_used_block_height"` // Tells the height of the most recent block with an output used in this transaction.
		MaxUsedBlockHash   string `json:"max_used_block_hash"`   // Tells the hash of the most recent block with an output used in this transaction.
		ReceiveTime        uint64 `json:"receive_time"`          // The Unix time that the transaction was first seen on the network by the node
		Relayed            bool   `json:"relayed"`               // States if this transaction has been relayed
		TxBlob             string `json:"tx_blob"`               // Hexadecimal blob represnting the transaction
		TxJson             string `json:"tx_json"`               // JSON structure of all information in the transaction
		TransactionDetail  TransactionDetail
	} `json:"transactions"` // List of transactions in the mempool are not in a block on the main chain at the moment
}

// ResponseGetTransactionPoolHashes  Get hashes from transaction pool. Binary request
type ResponseGetTransactionPoolHashes struct {
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
	TxHashes  []byte `json:"tx_hashes"` // binary array of transaction hashes.
}

// ResponseGetTransactionPoolStats Get the transaction pool statistics.
type ResponseGetTransactionPoolStats struct {
	Credit    uint64 `json:"credit"` // If payment for RPC is enabled, the number of credits available to the requesting client. Otherwise, 0.
	PoolStats struct {
		BytesMax   uint64 `json:"bytes_max"`   // Max transaction size in pool
		BytesMed   uint64 `json:"bytes_med"`   // Median transaction size in pool
		BytesMin   uint64 `json:"bytes_min"`   // Min transaction size in pool
		BytesTotal uint64 `json:"bytes_total"` // total size of all transactions in pool
		FeeTotal   uint64 `json:"fee_total"`   // The sum of the fees for all transactions currently in the transaction pool atomic units
		Histo      struct {
			Txs   uint64 `json:"txs"`   // number of transactions
			Bytes uint64 `json:"bytes"` // size in bytes.
		} `json:"histo"`
		Histo98PC       uint64 `json:"histo_98pc"`        // the time 98% of txes are "younger" than
		Num10M          uint64 `json:"num_10m"`           // number of transactions in pool for more than 10 minutes
		NumDoubleSpends uint64 `json:"num_double_spends"` // number of double spend transactions
		NumFailing      uint64 `json:"num_failing"`       // number of failing transactions
		NumNotRelayed   uint64 `json:"num_not_relayed"`   // number of non-relayed transactions
		Oldest          uint64 `json:"oldest"`            // unix time of the oldest transaction in the pool
		TxsTotal        uint64 `json:"txs_total"`         // total number of transactions.
	} `json:"pool_stats"`
	Status    string `json:"status"`    // General RPC error code. "OK" means everything looks good.
	TooBig    bool   `json:"too_big"`   // Transaction size is too big (true) or OK (false).
	UnTrusted bool   `json:"untrusted"` // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// ResponseGetLimit Get daemon bandwidth limits.
type ResponseGetLimit struct {
	LimitDown uint64 `json:"limit_down"` // Download limit in kBytes per second
	LimitUp   uint64 `json:"limit_up"`   // Upload limit in kBytes per second
	Status    string `json:"status"`     // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"`  // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}

// RequestSetLimit Set daemon bandwidth limits
type RequestSetLimit struct {
	LimitDown uint64 `json:"limit_down"` // Download limit in kBytes per second (-1 reset to default, 0 don't change the current limit)
	LimitUp   uint64 `json:"limit_up"`   // Upload limit in kBytes per second (-1 reset to default, 0 don't change the current limit)
}

// ResponseSetLimit response of  set daemon bandwidth limits.
type ResponseSetLimit struct {
	LimitDown uint64 `json:"limit_down"` // Download limit in kBytes per second
	LimitUp   uint64 `json:"limit_up"`   // Upload limit in kBytes per second
	Status    string `json:"status"`     // General RPC error code. "OK" means everything looks good.
	UnTrusted bool   `json:"untrusted"`  // States if the result is obtained using the bootstrap mode, and is therefore not trusted (true), or when the daemon is fully synced and thus handles the RPC locally (false)
}
