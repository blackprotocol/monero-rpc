package daemon

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/gorilla/rpc/v2/json2"

	rpc "gitlab.com/blackprotocol/monero-rpc"
)

// Client interface to access monero daemon
type Client interface {
	// GetBlockCount look up how many blocks are in the longest chain known to the node.
	GetBlockCount() (*ResponseGetBlockCount, error)
	// GetBlockHashByHeight look up a block's hash by its height.
	GetBlockHashByHeight(int64) (*string, error)
	// GetBlockTemplate Get a block template on which mining a new block.
	GetBlockTemplate(*RequestGetBlockTemplate) (*ResponseGetBlockTemplate, error)
	// SubmitBlock Submit a mined block to the network.
	SubmitBlock(block *RequestSubmitBlock) (*ResponseSubmitBlock, error)
	// GetLastBlockHeader Block header information for the most recent block
	GetLastBlockHeader() (*ResponseGetLastBlockHeader, error)
	// GetBlockHeaderByHash get_block_header_by_hash
	GetBlockHeaderByHash(*RequestGetBlockHeaderByHash) (*ResponseGetBlockHeaderByHash, error)
	// GetBlockHeaderByHeight get block header by height
	GetBlockHeaderByHeight(*RequestGetBlockHeaderByHeight) (*ResponseGetBlockHeaderByHeight, error)
	// GetBlockHeadersByRange get headers in the given range
	GetBlockHeadersByRange(*RequestGetBlockHeadersRange) (*ResponseGetBlockHeadersRange, error)
	// GetBlock get a block detail
	GetBlock(*RequestGetBlock) (*ResponseGetBlock, error)
	// GetConnections Retrieve information about incoming and outgoing connections to your node (get_connections)
	GetConnections() (*ResponseGetConnections, error)
	// GetInfo Retrieve general information about the state of your node and the network. (get_info)
	GetInfo() (*ResponseGetInfo, error)
	// HardforkInfo Look up information regarding hard fork voting and readiness.(hard_fork_info)
	HardforkInfo() (*ResponseHardForkInfo, error)
	// SetBan Ban another node by IP
	SetBan(*RequestSetBan) (*ResponseSetBan, error)
	// GetBans Get list of banned IPs
	GetBans() (*ResponseGetBans, error)
	// FlushTxPool Flush tx ids from transaction pool
	FlushTxPool(*RequestFlushTxPool) (*ResponseFlushTxPool, error)
	// GetOutputHistogram Get a histogram of output amounts. For all amounts (possibly filtered by parameters), gives the number of outputs on the chain for that amount. RingCT outputs counts as 0 amount.
	GetOutputHistogram(*RequestGetOutputHistogram) (*ResponseGetOutputHistogram, error)
	// GetCoinbaseTxSum Get the coinbase amount and the fees amount for n last blocks starting at particular height
	GetCoinbaseTxSum(*RequestGetCoinbaseTxSum) (*ResponseGetCoinbaseTxSum, error)
	// GetVersion Give the node current version
	GetVersion() (*ResponseGetVersion, error)
	// GetFeeEstimate Gives an estimation on fees per byte
	GetFeeEstimate(*RequestGetFeeEstimate) (*ResponseGetFeeEstimate, error)
	// GetAlternateChains Display alternative chains seen by the node.
	GetAlternateChains() (*ResponseGetAlternateChains, error)
	// RelayTx Relay a list of transaction IDs.
	RelayTx(*RequestRelayTx) (*ResponseRelayTx, error)
	// SyncInfo Get synchronisation information
	SyncInfo() (*ResponseSyncInfo, error)
	// GetTxPoolBacklog Get all transaction pool backlog
	GetTxPoolBacklog() (*ResponseGetTxPoolBacklog, error)
	// GetOutputDistribution (get_output_distribution)
	GetOutputDistribution(*RequestGetOutputDistribution) (*ResponseGetOutputDistribution, error)
	// GetTransactions get requested transactions
	GetTransactions(*RequestGetTransactions) (*ResponseGetTransactions, error)
	// IsKeyImageSpent Check if outputs have been spent using the key image associated with the output
	IsKeyImageSpent(*RequestIsKeyImageSpent) (*ResponseIsKeyImageSpent, error)
	// SendRawTransaction Broadcast a raw transaction to the network
	SendRawTransaction(*RequestSendRawTransaction) (*ResponseSendRawTransaction, error)
	// GetPeerList Get the known peers list
	GetPeerList() (*ResponseGetPeerList, error)
	// GetTransactionPool Show information about valid transactions seen by the node but not yet mined into a block, as well as spent key image information for the txpool in the node's memory.
	GetTransactionPool() (*ResponseGetTransactionPool, error)
	// GetTransactionPoolHashes Get hashes from transaction pool. Binary request.
	GetTransactionPoolHashes() (*ResponseGetTransactionPoolHashes, error)
	// GetTransactionPoolStats Get the transaction pool statistics
	GetTransactionPoolStats() (*ResponseGetTransactionPoolStats, error)
	// GetLimit Get daemon bandwidth limits.
	GetLimit() (*ResponseGetLimit, error)
	// SetLimit Set daemon bandwidth limits.
	SetLimit(*RequestSetLimit) (*ResponseSetLimit, error)
}

type client struct {
	httpClient *http.Client
	addr       string
	headers    map[string]string
}

// New returns a new monero-wallet-rpc client.
func New(cfg rpc.Config) Client {
	cl := &client{
		addr:    cfg.Address,
		headers: cfg.CustomHeaders,
	}
	if cfg.Transport == nil {
		cl.httpClient = http.DefaultClient
	} else {
		cl.httpClient = &http.Client{
			Transport: cfg.Transport,
		}
	}
	return cl
}
func (c *client) GetBlockCount() (resp *ResponseGetBlockCount, err error) {
	if err := c.do("get_block_count", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetBlockHashByHeight(height int64) (*string, error) {
	if height <= 0 {
		return nil, errors.New("invalid block height")
	}
	var result string
	if err := c.do("on_get_block_hash", []int64{height}, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
func (c *client) GetBlockTemplate(req *RequestGetBlockTemplate) (resp *ResponseGetBlockTemplate, err error) {
	if err := c.do("get_block_template", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) SubmitBlock(req *RequestSubmitBlock) (resp *ResponseSubmitBlock, err error) {
	if err := c.do("submit_block", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetLastBlockHeader() (resp *ResponseGetLastBlockHeader, err error) {
	if err := c.do("get_last_block_header", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetBlockHeaderByHash(hash *RequestGetBlockHeaderByHash) (resp *ResponseGetBlockHeaderByHash, err error) {
	if err := c.do("get_block_header_by_hash", hash, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetBlockHeaderByHeight(req *RequestGetBlockHeaderByHeight) (resp *ResponseGetBlockHeaderByHeight, err error) {
	if err := c.do("get_block_header_by_height", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetBlockHeadersByRange(req *RequestGetBlockHeadersRange) (resp *ResponseGetBlockHeadersRange, err error) {
	if err := c.do("get_block_headers_range", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetBlock(req *RequestGetBlock) (resp *ResponseGetBlock, err error) {
	if err := c.do("get_block", req, &resp); err != nil {
		return nil, err
	}
	var blockDetail BlockDetail
	if err := json.Unmarshal([]byte(resp.Json), &blockDetail); err != nil {
		return resp, fmt.Errorf("fail to unmarshal json field to block detail,err: %w", err)
	}
	resp.BlockDetail = blockDetail
	return
}
func (c *client) GetConnections() (resp *ResponseGetConnections, err error) {
	if err := c.do("get_connections", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetInfo() (resp *ResponseGetInfo, err error) {
	if err := c.do("get_info", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) HardforkInfo() (resp *ResponseHardForkInfo, err error) {
	if err := c.do("hard_fork_info", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) SetBan(req *RequestSetBan) (resp *ResponseSetBan, err error) {
	if err := c.do("set_ban", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetBans() (resp *ResponseGetBans, err error) {
	if err := c.do("get_bans", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) FlushTxPool(req *RequestFlushTxPool) (resp *ResponseFlushTxPool, err error) {
	if err := c.do("flush_txpool", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetOutputHistogram(req *RequestGetOutputHistogram) (resp *ResponseGetOutputHistogram, err error) {
	if err := c.do("get_output_histogram", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetCoinbaseTxSum(req *RequestGetCoinbaseTxSum) (resp *ResponseGetCoinbaseTxSum, err error) {
	if err := c.do("get_coinbase_tx_sum", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetVersion() (resp *ResponseGetVersion, err error) {
	if err := c.do("get_version", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetFeeEstimate(req *RequestGetFeeEstimate) (resp *ResponseGetFeeEstimate, err error) {
	if err := c.do("get_fee_estimate", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetAlternateChains() (resp *ResponseGetAlternateChains, err error) {
	if err := c.do("get_alternate_chains", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) RelayTx(req *RequestRelayTx) (resp *ResponseRelayTx, err error) {
	if err := c.do("relay_tx", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) SyncInfo() (resp *ResponseSyncInfo, err error) {
	if err := c.do("sync_info", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetTxPoolBacklog() (resp *ResponseGetTxPoolBacklog, err error) {
	if err := c.do("get_txpool_backlog", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetOutputDistribution(*RequestGetOutputDistribution) (resp *ResponseGetOutputDistribution, err error) {
	if err := c.do("get_output_distribution", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetTransactions(req *RequestGetTransactions) (resp *ResponseGetTransactions, err error) {
	if err := c.daemonRequest("get_transactions", req, &resp); err != nil {
		return nil, err
	}
	for idx, item := range resp.Txs {
		if len(item.AsJson) > 0 {
			var d TransactionDetail
			if err := json.Unmarshal([]byte(item.AsJson), &d); err != nil {
				return nil, fmt.Errorf("fail to unmarshal transaction detail,err: %w", err)
			}
			resp.Txs[idx].Detail = d
		}
	}
	return
}
func (c *client) IsKeyImageSpent(req *RequestIsKeyImageSpent) (resp *ResponseIsKeyImageSpent, err error) {
	if err := c.daemonRequest("is_key_image_spent", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) SendRawTransaction(req *RequestSendRawTransaction) (resp *ResponseSendRawTransaction, err error) {
	if err := c.daemonRequest("send_raw_transaction", req, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetPeerList() (resp *ResponseGetPeerList, err error) {
	if err := c.daemonRequest("get_peer_list", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetTransactionPool() (resp *ResponseGetTransactionPool, err error) {
	if err := c.daemonRequest("get_transaction_pool", nil, &resp); err != nil {
		return nil, err
	}
	for idx, item := range resp.Transactions {
		if len(item.TxJson) > 0 {
			var detail TransactionDetail
			if err := json.Unmarshal([]byte(item.TxJson), &detail); err != nil {
				return nil, fmt.Errorf("fail to unmarshal transaction detail,err: %w", err)
			}
			resp.Transactions[idx].TransactionDetail = detail
		}
	}
	return
}
func (c *client) GetTransactionPoolHashes() (resp *ResponseGetTransactionPoolHashes, err error) {
	if err := c.daemonRequest("get_transaction_pool_hashes.bin", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetTransactionPoolStats() (resp *ResponseGetTransactionPoolStats, err error) {
	if err := c.daemonRequest("get_transaction_pool_stats", nil, &resp); err != nil {
		return nil, err
	}
	return
}
func (c *client) GetLimit() (resp *ResponseGetLimit, err error) {
	if err := c.daemonRequest("get_limit", nil, &resp); err != nil {
		return nil, err
	}
	return
}

func (c *client) SetLimit(req *RequestSetLimit) (resp *ResponseSetLimit, err error) {
	if err := c.daemonRequest("set_limit", req, &resp); err != nil {
		return nil, err
	}
	return
}

func (c *client) daemonRequest(method string, in, out any) error {
	url, err := c.getUrl(method)
	if err != nil {
		return fmt.Errorf("fail to get request url,err:%w", err)
	}
	var b io.Reader
	if in != nil {
		buf, err := json.Marshal(in)
		if err != nil {
			return fmt.Errorf("fail to marshal request,err:%w", err)
		}
		b = bytes.NewBuffer(buf)
	} else {
		b = http.NoBody
	}
	httpReq, err := http.NewRequest(http.MethodPost, url, b)
	if err != nil {
		return fmt.Errorf("fail to create request,err: %w", err)
	}
	resp, err := c.httpClient.Do(httpReq)
	if err != nil {
		return fmt.Errorf("fail to send http request,err: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {

		}
	}()
	dec := json.NewDecoder(resp.Body)
	return dec.Decode(out)
}
func (c *client) getUrl(method string) (string, error) {
	u, err := url.Parse(c.addr)
	if err != nil {
		return "", fmt.Errorf("fail to parse address,err: %w", err)
	}
	return fmt.Sprintf("http://%s/%s", u.Host, method), nil
}

// Helper function
func (c *client) do(method string, in, out any) (err error) {
	payload, err := json2.EncodeClientRequest(method, in)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, c.addr, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}

	req.Close = true
	if c.headers != nil {
		for k, v := range c.headers {
			req.Header.Set(k, v)
		}
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("http status %v", resp.StatusCode)
	}
	defer func() {
		if errClose := resp.Body.Close(); errClose != nil {
		}
	}()

	// in theory this is only done to catch
	// any monero related errors if
	// we are not expecting any data back
	if out == nil {
		v := &json2.EmptyResponse{}
		return json2.DecodeClientResponse(resp.Body, v)
	}

	return json2.DecodeClientResponse(resp.Body, out)
}
